namespace Feliz.MaterialUI.X

open System.ComponentModel
open Fable.Core
open Fable.Core.JsInterop
open Fable.React

[<Erase>]
type MuiComponents =
  static member inline dateCalendar : ReactElementType = import "DateCalendar" "@mui/x-date-pickers/DateCalendar"

  static member inline dateField : ReactElementType = import "DateField" "@mui/x-date-pickers/DateField"

  static member inline datePicker : ReactElementType = import "DatePicker" "@mui/x-date-pickers/DatePicker"

  static member inline datePickerToolbar : ReactElementType = import "DatePickerToolbar" "@mui/x-date-pickers"

  static member inline dateTimeField : ReactElementType = import "DateTimeField" "@mui/x-date-pickers/DateTimeField"

  static member inline dateTimePicker : ReactElementType = import "DateTimePicker" "@mui/x-date-pickers/DateTimePicker"

  static member inline dateTimePickerTabs : ReactElementType = import "DateTimePickerTabs" "@mui/x-date-pickers"

  static member inline dateTimePickerToolbar : ReactElementType = import "DateTimePickerToolbar" "@mui/x-date-pickers"

  static member inline dayCalendarSkeleton : ReactElementType = import "DayCalendarSkeleton" "@mui/x-date-pickers/DayCalendarSkeleton"

  static member inline desktopDatePicker : ReactElementType = import "DesktopDatePicker" "@mui/x-date-pickers/DesktopDatePicker"

  static member inline desktopDateTimePicker : ReactElementType = import "DesktopDateTimePicker" "@mui/x-date-pickers/DesktopDateTimePicker"

  static member inline desktopTimePicker : ReactElementType = import "DesktopTimePicker" "@mui/x-date-pickers/DesktopTimePicker"

  static member inline digitalClock : ReactElementType = import "DigitalClock" "@mui/x-date-pickers/DigitalClock"

  static member inline localizationProvider : ReactElementType = import "LocalizationProvider" "@mui/x-date-pickers/LocalizationProvider"

  static member inline mobileDatePicker : ReactElementType = import "MobileDatePicker" "@mui/x-date-pickers/MobileDatePicker"

  static member inline mobileDateTimePicker : ReactElementType = import "MobileDateTimePicker" "@mui/x-date-pickers/MobileDateTimePicker"

  static member inline mobileTimePicker : ReactElementType = import "MobileTimePicker" "@mui/x-date-pickers/MobileTimePicker"

  static member inline monthCalendar : ReactElementType = import "MonthCalendar" "@mui/x-date-pickers/MonthCalendar"

  static member inline multiSectionDigitalClock : ReactElementType = import "MultiSectionDigitalClock" "@mui/x-date-pickers/MultiSectionDigitalClock"

  static member inline pickersActionBar : ReactElementType = import "PickersActionBar" "@mui/x-date-pickers/PickersActionBar"

  static member inline pickersDay : ReactElementType = import "PickersDay" "@mui/x-date-pickers/PickersDay"

  static member inline pickersLayout : ReactElementType = import "PickersLayout" "@mui/x-date-pickers/PickersLayout"

  static member inline pickersShortcuts : ReactElementType = import "PickersShortcuts" "@mui/x-date-pickers/PickersShortcuts"

  static member inline staticDatePicker : ReactElementType = import "StaticDatePicker" "@mui/x-date-pickers/StaticDatePicker"

  static member inline staticDateTimePicker : ReactElementType = import "StaticDateTimePicker" "@mui/x-date-pickers/StaticDateTimePicker"

  static member inline staticTimePicker : ReactElementType = import "StaticTimePicker" "@mui/x-date-pickers/StaticTimePicker"

  static member inline timeClock : ReactElementType = import "TimeClock" "@mui/x-date-pickers/TimeClock"

  static member inline timeField : ReactElementType = import "TimeField" "@mui/x-date-pickers/TimeField"

  static member inline timePicker : ReactElementType = import "TimePicker" "@mui/x-date-pickers/TimePicker"

  static member inline timePickerToolbar : ReactElementType = import "TimePickerToolbar" "@mui/x-date-pickers"

  static member inline yearCalendar : ReactElementType = import "YearCalendar" "@mui/x-date-pickers/YearCalendar"
