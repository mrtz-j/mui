module Feliz.MaterialUI.X.MuiClasses

open System.ComponentModel
open Fable.Core
open Feliz
open Feliz.MaterialUI

type [<Erase>] dateCalendar =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiDateCalendar-root"
  /// Styles applied to the transition group element.
  static member inline viewTransitionContainer : string = ".MuiDateCalendar-viewTransitionContainer"


type [<Erase>] dateField =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiDateField-root"


type [<Erase>] datePickerToolbar =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiDatePickerToolbar-root"
  /// Styles applied to the title element.
  static member inline title : string = ".MuiDatePickerToolbar-title"


type [<Erase>] dateTimeField =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiDateTimeField-root"


type [<Erase>] dateTimePickerTabs =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiDateTimePickerTabs-root"


type [<Erase>] dateTimePickerToolbar =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiDateTimePickerToolbar-root"
  /// Styles applied to the date container element.
  static member inline dateContainer : string = ".MuiDateTimePickerToolbar-dateContainer"
  /// Styles applied to the time container element.
  static member inline timeContainer : string = ".MuiDateTimePickerToolbar-timeContainer"
  /// Styles applied to the time (except am/pm) container element.
  static member inline timeDigitsContainer : string = ".MuiDateTimePickerToolbar-timeDigitsContainer"
  /// Styles applied to the time container if rtl.
  static member inline timeLabelReverse : string = ".MuiDateTimePickerToolbar-timeLabelReverse"
  /// Styles applied to the separator element.
  static member inline separator : string = ".MuiDateTimePickerToolbar-separator"
  /// Styles applied to the am/pm section.
  static member inline ampmSelection : string = ".MuiDateTimePickerToolbar-ampmSelection"
  /// Styles applied to am/pm section in landscape mode.
  static member inline ampmLandscape : string = ".MuiDateTimePickerToolbar-ampmLandscape"
  /// Styles applied to am/pm labels.
  static member inline ampmLabel : string = ".MuiDateTimePickerToolbar-ampmLabel"


type [<Erase>] dayCalendarSkeleton =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiDayCalendarSkeleton-root"
  /// Styles applied to the week element.
  static member inline week : string = ".MuiDayCalendarSkeleton-week"
  /// Styles applied to the day element.
  static member inline daySkeleton : string = ".MuiDayCalendarSkeleton-daySkeleton"


type [<Erase>] digitalClock =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiDigitalClock-root"
  /// Styles applied to the list (by default: MenuList) element.
  static member inline list : string = ".MuiDigitalClock-list"
  /// Styles applied to the list item (by default: MenuItem) element.
  static member inline item : string = ".MuiDigitalClock-item"


type [<Erase>] monthCalendar =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiMonthCalendar-root"


type [<Erase>] multiSectionDigitalClock =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiMultiSectionDigitalClock-root"


type [<Erase>] pickersActionBar =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiPickersActionBar-root"
  /// Styles applied to the root element unless `disableSpacing={true}`.
  static member inline spacing : string = ".MuiPickersActionBar-spacing"


type [<Erase>] pickersDay =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiPickersDay-root"
  /// Styles applied to the root element if `disableMargin=false`.
  static member inline dayWithMargin : string = ".MuiPickersDay-dayWithMargin"
  /// Styles applied to the root element if `outsideCurrentMonth=true` and `showDaysOutsideCurrentMonth=true`.
  static member inline dayOutsideMonth : string = ".MuiPickersDay-dayOutsideMonth"
  /// Styles applied to the root element if `outsideCurrentMonth=true` and `showDaysOutsideCurrentMonth=false`.
  static member inline hiddenDaySpacingFiller : string = ".MuiPickersDay-hiddenDaySpacingFiller"
  /// Styles applied to the root element if `disableHighlightToday=false` and `today=true`.
  static member inline today : string = ".MuiPickersDay-today"
  /// State class applied to the root element if `selected=true`.
  static member inline selected : string = ".Mui-selected"
  /// State class applied to the root element if `disabled=true`.
  static member inline disabled : string = ".Mui-disabled"


type [<Erase>] pickersLayout =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiPickersLayout-root"
  /// Styles applied to the root element in landscape orientation.
  static member inline landscape : string = ".MuiPickersLayout-landscape"
  /// Styles applied to the contentWrapper element (which contains the tabs and the view itself).
  static member inline contentWrapper : string = ".MuiPickersLayout-contentWrapper"
  /// Styles applied to the toolbar.
  static member inline toolbar : string = ".MuiPickersLayout-toolbar"
  /// Styles applied to the action bar.
  static member inline actionBar : string = ".MuiPickersLayout-actionBar"
  /// Styles applied to the tabs.
  static member inline tabs : string = ".MuiPickersLayout-tabs"
  /// Styles applied to the shortcuts container.
  static member inline shortcuts : string = ".MuiPickersLayout-shortcuts"


type [<Erase>] pickersShortcuts =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiPickersShortcuts-root"
  /// Styles applied to the root element unless `disablePadding={true}`.
  static member inline padding : string = ".MuiPickersShortcuts-padding"
  /// Styles applied to the root element if dense.
  static member inline dense : string = ".MuiPickersShortcuts-dense"
  /// Styles applied to the root element if a `subheader` is provided.
  static member inline subheader : string = ".MuiPickersShortcuts-subheader"


type [<Erase>] timeClock =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiTimeClock-root"
  /// Styles applied to the arrowSwitcher element.
  static member inline arrowSwitcher : string = ".MuiTimeClock-arrowSwitcher"


type [<Erase>] timeField =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiTimeField-root"


type [<Erase>] timePickerToolbar =

  static member inline root : string = ".MuiTimePickerToolbar-root"
  static member inline separator : string = ".MuiTimePickerToolbar-separator"
  static member inline hourMinuteLabel : string = ".MuiTimePickerToolbar-hourMinuteLabel"
  static member inline hourMinuteLabelLandscape : string = ".MuiTimePickerToolbar-hourMinuteLabelLandscape"
  static member inline hourMinuteLabelReverse : string = ".MuiTimePickerToolbar-hourMinuteLabelReverse"
  static member inline ampmSelection : string = ".MuiTimePickerToolbar-ampmSelection"
  static member inline ampmLandscape : string = ".MuiTimePickerToolbar-ampmLandscape"
  static member inline ampmLabel : string = ".MuiTimePickerToolbar-ampmLabel"


type [<Erase>] yearCalendar =

  /// Styles applied to the root element.
  static member inline root : string = ".MuiYearCalendar-root"

