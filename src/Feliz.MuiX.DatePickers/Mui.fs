namespace Feliz.MaterialUI.X

open System.ComponentModel
open Fable.Core
open Fable.Core.JsInterop
open Feliz
open Feliz.MaterialUI

[<AutoOpen; EditorBrowsable(EditorBrowsableState.Never)>]
module MuiXHelpers =

  open Fable.React

  let reactElement (el: ReactElementType) (props: 'a) : ReactElement =
    import "createElement" "react"

  let reactElementTag (tag: string) (props: 'a) : ReactElement =
    import "createElement" "react"

  let createElement (el: ReactElementType) (properties: IReactProperty seq) : ReactElement =
    reactElement el (!!properties |> Object.fromFlatEntries)

  let createElementTag (tag: string) (properties: IReactProperty seq) : ReactElement =
    reactElementTag tag (!!properties |> Object.fromFlatEntries)

[<Erase>]
type MuiX =

  static member inline dateCalendar props = createElement (import "DateCalendar" "@mui/x-date-pickers/DateCalendar") props

  static member inline dateField props = createElement (import "DateField" "@mui/x-date-pickers/DateField") props

  static member inline datePicker props = createElement (import "DatePicker" "@mui/x-date-pickers/DatePicker") props

  static member inline datePickerToolbar props = createElement (import "DatePickerToolbar" "@mui/x-date-pickers") props

  static member inline dateTimeField props = createElement (import "DateTimeField" "@mui/x-date-pickers/DateTimeField") props

  static member inline dateTimePicker props = createElement (import "DateTimePicker" "@mui/x-date-pickers/DateTimePicker") props

  static member inline dateTimePickerTabs props = createElement (import "DateTimePickerTabs" "@mui/x-date-pickers") props

  static member inline dateTimePickerToolbar props = createElement (import "DateTimePickerToolbar" "@mui/x-date-pickers") props

  static member inline dayCalendarSkeleton props = createElement (import "DayCalendarSkeleton" "@mui/x-date-pickers/DayCalendarSkeleton") props

  static member inline desktopDatePicker props = createElement (import "DesktopDatePicker" "@mui/x-date-pickers/DesktopDatePicker") props

  static member inline desktopDateTimePicker props = createElement (import "DesktopDateTimePicker" "@mui/x-date-pickers/DesktopDateTimePicker") props

  static member inline desktopTimePicker props = createElement (import "DesktopTimePicker" "@mui/x-date-pickers/DesktopTimePicker") props

  static member inline digitalClock props = createElement (import "DigitalClock" "@mui/x-date-pickers/DigitalClock") props

  static member inline localizationProvider props = createElement (import "LocalizationProvider" "@mui/x-date-pickers/LocalizationProvider") props

  static member inline mobileDatePicker props = createElement (import "MobileDatePicker" "@mui/x-date-pickers/MobileDatePicker") props

  static member inline mobileDateTimePicker props = createElement (import "MobileDateTimePicker" "@mui/x-date-pickers/MobileDateTimePicker") props

  static member inline mobileTimePicker props = createElement (import "MobileTimePicker" "@mui/x-date-pickers/MobileTimePicker") props

  static member inline monthCalendar props = createElement (import "MonthCalendar" "@mui/x-date-pickers/MonthCalendar") props

  static member inline multiSectionDigitalClock props = createElement (import "MultiSectionDigitalClock" "@mui/x-date-pickers/MultiSectionDigitalClock") props

  static member inline pickersActionBar props = createElement (import "PickersActionBar" "@mui/x-date-pickers/PickersActionBar") props

  static member inline pickersDay props = createElement (import "PickersDay" "@mui/x-date-pickers/PickersDay") props

  static member inline pickersLayout props = createElement (import "PickersLayout" "@mui/x-date-pickers/PickersLayout") props

  static member inline pickersShortcuts props = createElement (import "PickersShortcuts" "@mui/x-date-pickers/PickersShortcuts") props

  static member inline staticDatePicker props = createElement (import "StaticDatePicker" "@mui/x-date-pickers/StaticDatePicker") props

  static member inline staticDateTimePicker props = createElement (import "StaticDateTimePicker" "@mui/x-date-pickers/StaticDateTimePicker") props

  static member inline staticTimePicker props = createElement (import "StaticTimePicker" "@mui/x-date-pickers/StaticTimePicker") props

  static member inline timeClock props = createElement (import "TimeClock" "@mui/x-date-pickers/TimeClock") props

  static member inline timeField props = createElement (import "TimeField" "@mui/x-date-pickers/TimeField") props

  static member inline timePicker props = createElement (import "TimePicker" "@mui/x-date-pickers/TimePicker") props

  static member inline timePickerToolbar props = createElement (import "TimePickerToolbar" "@mui/x-date-pickers") props

  static member inline yearCalendar props = createElement (import "YearCalendar" "@mui/x-date-pickers/YearCalendar") props
