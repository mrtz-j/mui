namespace Feliz.MaterialUI.X

open System.ComponentModel
open Fable.Core
open Fable.Core.JsInterop
open Feliz
open Feliz.MaterialUI

[<AutoOpen; EditorBrowsable(EditorBrowsableState.Never)>]
module themeProps =

  module theme =

    [<Erase>]
    type defaultProps =
      static member inline muiDateCalendar(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDateCalendar.defaultProps", createObj !!props)
      static member inline muiDateField(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDateField.defaultProps", createObj !!props)
      static member inline muiDatePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDatePicker.defaultProps", createObj !!props)
      static member inline muiDatePickerToolbar(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDatePickerToolbar.defaultProps", createObj !!props)
      static member inline muiDateTimeField(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDateTimeField.defaultProps", createObj !!props)
      static member inline muiDateTimePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDateTimePicker.defaultProps", createObj !!props)
      static member inline muiDateTimePickerTabs(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDateTimePickerTabs.defaultProps", createObj !!props)
      static member inline muiDateTimePickerToolbar(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.defaultProps", createObj !!props)
      static member inline muiDayCalendarSkeleton(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDayCalendarSkeleton.defaultProps", createObj !!props)
      static member inline muiDesktopDatePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDesktopDatePicker.defaultProps", createObj !!props)
      static member inline muiDesktopDateTimePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDesktopDateTimePicker.defaultProps", createObj !!props)
      static member inline muiDesktopTimePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDesktopTimePicker.defaultProps", createObj !!props)
      static member inline muiDigitalClock(props: IReactProperty list) : IThemeProp = unbox ("components.MuiDigitalClock.defaultProps", createObj !!props)
      static member inline muiLocalizationProvider(props: IReactProperty list) : IThemeProp = unbox ("components.MuiLocalizationProvider.defaultProps", createObj !!props)
      static member inline muiMobileDatePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiMobileDatePicker.defaultProps", createObj !!props)
      static member inline muiMobileDateTimePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiMobileDateTimePicker.defaultProps", createObj !!props)
      static member inline muiMobileTimePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiMobileTimePicker.defaultProps", createObj !!props)
      static member inline muiMonthCalendar(props: IReactProperty list) : IThemeProp = unbox ("components.MuiMonthCalendar.defaultProps", createObj !!props)
      static member inline muiMultiSectionDigitalClock(props: IReactProperty list) : IThemeProp = unbox ("components.MuiMultiSectionDigitalClock.defaultProps", createObj !!props)
      static member inline muiPickersActionBar(props: IReactProperty list) : IThemeProp = unbox ("components.MuiPickersActionBar.defaultProps", createObj !!props)
      static member inline muiPickersDay(props: IReactProperty list) : IThemeProp = unbox ("components.MuiPickersDay.defaultProps", createObj !!props)
      static member inline muiPickersLayout(props: IReactProperty list) : IThemeProp = unbox ("components.MuiPickersLayout.defaultProps", createObj !!props)
      static member inline muiPickersShortcuts(props: IReactProperty list) : IThemeProp = unbox ("components.MuiPickersShortcuts.defaultProps", createObj !!props)
      static member inline muiStaticDatePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiStaticDatePicker.defaultProps", createObj !!props)
      static member inline muiStaticDateTimePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiStaticDateTimePicker.defaultProps", createObj !!props)
      static member inline muiStaticTimePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiStaticTimePicker.defaultProps", createObj !!props)
      static member inline muiTimeClock(props: IReactProperty list) : IThemeProp = unbox ("components.MuiTimeClock.defaultProps", createObj !!props)
      static member inline muiTimeField(props: IReactProperty list) : IThemeProp = unbox ("components.MuiTimeField.defaultProps", createObj !!props)
      static member inline muiTimePicker(props: IReactProperty list) : IThemeProp = unbox ("components.MuiTimePicker.defaultProps", createObj !!props)
      static member inline muiTimePickerToolbar(props: IReactProperty list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.defaultProps", createObj !!props)
      static member inline muiYearCalendar(props: IReactProperty list) : IThemeProp = unbox ("components.MuiYearCalendar.defaultProps", createObj !!props)
