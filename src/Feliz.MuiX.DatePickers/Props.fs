namespace Feliz.MaterialUI.X

open System
open System.ComponentModel
open Browser.Types
open Fable.Core
open Fable.Core.JsInterop
open Fable.React
open Feliz


open Feliz.MaterialUI

[<Erase>]
type private Helpers =
  static member inline themeStylesOverride (callback: Theme -> #seq<IStyleAttribute>): 't =
    !!(Func<Theme, _> (stylesCallback !!callback))

  static member inline breakpointThemeStylesOverrides (overrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) =
    overrides
    |> Array.map (fun (breakpoint, themeOverride) -> string breakpoint, Helpers.themeStylesOverride themeOverride)
    |> !!createObj

  static member inline themeBreakpointStylesOverrides (overrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) =
    overrides |> Array.map (fun themeBpOverride ->
      Func<Theme, _> (fun theme ->
        let bpStyles = themeBpOverride theme |> List.map (fun (bp, styles) -> style.breakpoint bp styles)
        createObj !!bpStyles))

  static member inline renderElementCallback (renderer: #seq<IReactProperty> -> ReactElement): Func<'Props, ReactElement> =
    let outputCallback (propsObj: 'Props)  =
      let propsArray = JS.Constructors.Object.keys propsObj
      renderer (!!propsArray)
    Func<'Props, ReactElement> outputCallback

  static member inline renderElementCallback2 (renderer: #seq<IReactProperty> -> #seq<IReactProperty> -> ReactElement): Func<'Props, 'Props, ReactElement> =
    let outputCallback (propsObj1: 'Props) (propsObj2: 'Props)  =
      let propsArray1 = JS.Constructors.Object.entries propsObj1
      let propsArray2 = JS.Constructors.Object.entries propsObj2
      renderer (!!propsArray1) (!!propsArray2)
    Func<_, _, _> outputCallback


[<Erase>]
type dateCalendar =
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (value: Func<obj, obj>) = Interop.mkAttr "dayOfWeekFormatter" value
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default selected value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate | null, selectionState: PickerSelectionState | undefined) => void`
  ///
  /// *value:* The new value.
  ///
  /// *selectionState:* Indicates if the date selection is complete.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// Callback fired on focused view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView, hasFocus: boolean) => void`
  ///
  /// *view:* The new view to focus or not.
  ///
  /// *hasFocus:*`true` if the view should be focused.
  static member inline onFocusedViewChange (handler: Event -> unit) = Interop.mkAttr "onFocusedViewChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: Event -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: Event -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: Event -> unit) = Interop.mkAttr "onYearChange" handler
  /// Make picker read only.
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// The date used to generate the new value when both `value` and `defaultValue` are empty.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableDate" value
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableMonth" value
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableYear" value
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Available views.
  static member inline views ([<ParamArray>] values: string []) = Interop.mkAttr "views" values
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module dateCalendar =

  /// Controlled focused view.
  [<Erase>]
  type focusedView =
    static member inline day = Interop.mkAttr "focusedView" "day"
    static member inline month = Interop.mkAttr "focusedView" "month"
    static member inline year = Interop.mkAttr "focusedView" "year"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type dateField =
  /// If `true`, the `input` element is focused during the first mount.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default value. Use when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the component is disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the component is displayed in focused state.
  static member inline focused (value: bool) = Interop.mkAttr "focused" value
  /// Format of the date when rendered in the input(s).
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Props applied to the
  ///
  ///   [`FormHelperText`](https://mui.com/material-ui/api/form-helper-text/) element.
  static member inline FormHelperTextProps (props: IReactProperty list) = Interop.mkAttr "FormHelperTextProps" (createObj !!props)
  /// If `true`, the input will take up the full width of its container.
  static member inline fullWidth (value: bool) = Interop.mkAttr "fullWidth" value
  /// The helper text content.
  static member inline helperText (value: ReactElement) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (values: seq<ReactElement>) = Interop.mkAttr "helperText" values
  /// The helper text content.
  static member inline helperText (value: string) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (values: string seq) = Interop.mkAttr "helperText" values
  /// The helper text content.
  static member inline helperText (value: int) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (value: float) = Interop.mkAttr "helperText" value
  /// If `true`, the label is hidden. This is used to increase density for a `FilledInput`. Be sure to add `aria-label` to the `input` element.
  static member inline hiddenLabel (value: bool) = Interop.mkAttr "hiddenLabel" value
  /// The id of the `input` element. Use this prop to make `label` and `helperText` accessible for screen readers.
  static member inline id (value: string) = Interop.mkAttr "id" value
  /// Props applied to the
  ///
  ///   [`InputLabel`](https://mui.com/material-ui/api/input-label/) element. Pointer events like `onClick` are enabled if and only if `shrink` is `true`.
  static member inline InputLabelProps (props: IReactProperty list) = Interop.mkAttr "InputLabelProps" (createObj !!props)
  /// [Attributes](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Attributes) applied to the `input` element.
  static member inline inputProps (props: IReactProperty list) = Interop.mkAttr "inputProps" (createObj !!props)
  /// Props applied to the Input element. It will be a
  ///
  ///   [`FilledInput`](https://mui.com/material-ui/api/filled-input/),
  ///
  ///   [`OutlinedInput`](https://mui.com/material-ui/api/outlined-input/) or
  ///
  ///   [`Input`](https://mui.com/material-ui/api/input/) component depending on the `variant` prop value.
  static member inline InputProps (props: IReactProperty list) = Interop.mkAttr "InputProps" (createObj !!props)
  /// Pass a ref to the `input` element.
  static member inline inputRef (value: Func<obj, obj>) = Interop.mkAttr "inputRef" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (current: 'T) = Interop.mkAttr "inputRef" (let x = createEmpty<obj> in x?``current`` <- current; x)
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Name attribute of the `input` element.
  static member inline name (value: string) = Interop.mkAttr "name" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// Callback fired when the error associated to the current value changes.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: Event -> unit) = Interop.mkAttr "onError" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// It prevents the user from changing the value of the field (not from interacting with the field).
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// The date used to generate a part of the new value that is not present in the format when both `value` and `defaultValue` are empty. For example, on time fields it will be used to determine the date to set.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// If `true`, the label is displayed as required and the `input` element is required.
  static member inline required (value: bool) = Interop.mkAttr "required" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableDate" value
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableMonth" value
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableYear" value
  /// If `true`, the format will respect the leading zeroes (e.g: on dayjs, the format `M/D/YYYY` will render `8/16/2018`) If `false`, the format will always add leading zeroes (e.g: on dayjs, the format `M/D/YYYY` will render `08/16/2018`)
  ///
  /// Warning n°1: Luxon is not able to respect the leading zeroes when using macro tokens (e.g: "DD"), so `shouldRespectLeadingZeros={true}` might lead to inconsistencies when using `AdapterLuxon`.
  ///
  /// Warning n°2: When `shouldRespectLeadingZeros={true}`, the field will add an invisible character on the sections containing a single digit to make sure `onChange` is fired. If you need to get the clean value from the input, you can remove this character using `input.value.replace(/\u200e/g, '')`.
  ///
  /// Warning n°3: When used in strict mode, dayjs and moment require to respect the leading zeros. This mean that when using `shouldRespectLeadingZeros={false}`, if you retrieve the value directly from the input (not listening to `onChange`) and your format contains tokens without leading zeros, the value will not be parsed by your library.
  static member inline shouldRespectLeadingZeros (value: bool) = Interop.mkAttr "shouldRespectLeadingZeros" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The ref object used to imperatively interact with the field.
  static member inline unstableFieldRef (value: Func<obj, obj>) = Interop.mkAttr "unstableFieldRef" value
  /// The ref object used to imperatively interact with the field.
  static member inline unstableFieldRef (value: obj) = Interop.mkAttr "unstableFieldRef" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module dateField =

  /// The color of the component. It supports both default and custom theme colors, which can be added as shown in the [palette customization guide](https://mui.com/material-ui/customization/palette/#adding-new-colors).
  [<Erase>]
  type color =
    static member inline error = Interop.mkAttr "color" "error"
    static member inline info = Interop.mkAttr "color" "info"
    static member inline primary = Interop.mkAttr "color" "primary"
    static member inline secondary = Interop.mkAttr "color" "secondary"
    static member inline success = Interop.mkAttr "color" "success"
    static member inline warning = Interop.mkAttr "color" "warning"

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// If `dense` or `normal`, will adjust vertical spacing of this and contained components.
  [<Erase>]
  type margin =
    static member inline dense = Interop.mkAttr "margin" "dense"
    static member inline none = Interop.mkAttr "margin" "none"
    static member inline normal = Interop.mkAttr "margin" "normal"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The size of the component.
  [<Erase>]
  type size =
    static member inline medium = Interop.mkAttr "size" "medium"
    static member inline small = Interop.mkAttr "size" "small"

  /// The variant to use.
  [<Erase>]
  type variant =
    static member inline filled = Interop.mkAttr "variant" "filled"
    static member inline outlined = Interop.mkAttr "variant" "outlined"
    static member inline standard = Interop.mkAttr "variant" "standard"


[<Erase>]
type datePicker =
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (formatter: string -> string) = Interop.mkAttr "dayOfWeekFormatter" (Func<_, _> formatter)
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// CSS media query when `Mobile` mode will be changed to `Desktop`.
  static member inline desktopModeMediaQuery (value: string) = Interop.mkAttr "desktopModeMediaQuery" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> JS.Promise<unit>) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: CalendarPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: 'TDate -> unit) = Interop.mkAttr "onYearChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (shouldDisableDate: System.DateTime -> bool) = Interop.mkAttr "shouldDisableDate" (Func<_, _> shouldDisableDate)
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (shouldDisableMonth: int -> bool) = Interop.mkAttr "shouldDisableMonth" (Func<_, _> shouldDisableMonth)
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (shouldDisableYear: int -> bool) = Interop.mkAttr "shouldDisableYear" (Func<_, _> shouldDisableYear)
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?day: Func<obj, obj>, ?month: Func<obj, obj>, ?year: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if day.IsSome then x?``day`` <- day.Value); (if month.IsSome then x?``month`` <- month.Value); (if year.IsSome then x?``year`` <- year.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: DatePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module datePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type datePickerToolbar =
  /// Callback called when a toolbar is clicked
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The view to open
  static member inline onViewChange (handler: Event -> unit) = Interop.mkAttr "onViewChange" handler
  /// className applied to the root component.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, show the toolbar even in desktop mode.
  static member inline hidden (value: bool) = Interop.mkAttr "hidden" value
  /// Toolbar date format.
  static member inline toolbarFormat (value: string) = Interop.mkAttr "toolbarFormat" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: ReactElement) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (values: seq<ReactElement>) = Interop.mkAttr "toolbarPlaceholder" values
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: string) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (values: string seq) = Interop.mkAttr "toolbarPlaceholder" values
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: int) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: float) = Interop.mkAttr "toolbarPlaceholder" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module datePickerToolbar =

  /// Currently visible picker view.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline year = Interop.mkAttr "view" "year"


[<Erase>]
type dateTimeField =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// If `true`, the `input` element is focused during the first mount.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default value. Use when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the component is disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the component is displayed in focused state.
  static member inline focused (value: bool) = Interop.mkAttr "focused" value
  /// Format of the date when rendered in the input(s).
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Props applied to the
  ///
  ///   [`FormHelperText`](https://mui.com/material-ui/api/form-helper-text/) element.
  static member inline FormHelperTextProps (props: IReactProperty list) = Interop.mkAttr "FormHelperTextProps" (createObj !!props)
  /// If `true`, the input will take up the full width of its container.
  static member inline fullWidth (value: bool) = Interop.mkAttr "fullWidth" value
  /// The helper text content.
  static member inline helperText (value: ReactElement) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (values: seq<ReactElement>) = Interop.mkAttr "helperText" values
  /// The helper text content.
  static member inline helperText (value: string) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (values: string seq) = Interop.mkAttr "helperText" values
  /// The helper text content.
  static member inline helperText (value: int) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (value: float) = Interop.mkAttr "helperText" value
  /// If `true`, the label is hidden. This is used to increase density for a `FilledInput`. Be sure to add `aria-label` to the `input` element.
  static member inline hiddenLabel (value: bool) = Interop.mkAttr "hiddenLabel" value
  /// The id of the `input` element. Use this prop to make `label` and `helperText` accessible for screen readers.
  static member inline id (value: string) = Interop.mkAttr "id" value
  /// Props applied to the
  ///
  ///   [`InputLabel`](https://mui.com/material-ui/api/input-label/) element. Pointer events like `onClick` are enabled if and only if `shrink` is `true`.
  static member inline InputLabelProps (props: IReactProperty list) = Interop.mkAttr "InputLabelProps" (createObj !!props)
  /// [Attributes](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Attributes) applied to the `input` element.
  static member inline inputProps (props: IReactProperty list) = Interop.mkAttr "inputProps" (createObj !!props)
  /// Props applied to the Input element. It will be a
  ///
  ///   [`FilledInput`](https://mui.com/material-ui/api/filled-input/),
  ///
  ///   [`OutlinedInput`](https://mui.com/material-ui/api/outlined-input/) or
  ///
  ///   [`Input`](https://mui.com/material-ui/api/input/) component depending on the `variant` prop value.
  static member inline InputProps (props: IReactProperty list) = Interop.mkAttr "InputProps" (createObj !!props)
  /// Pass a ref to the `input` element.
  static member inline inputRef (value: Func<obj, obj>) = Interop.mkAttr "inputRef" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (current: 'T) = Interop.mkAttr "inputRef" (let x = createEmpty<obj> in x?``current`` <- current; x)
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Maximal selectable moment of time with binding to date, to set max time in each day use `maxTime`.
  static member inline maxDateTime (value: 'T) = Interop.mkAttr "maxDateTime" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Minimal selectable moment of time with binding to date, to set min time in each day use `minTime`.
  static member inline minDateTime (value: 'T) = Interop.mkAttr "minDateTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Name attribute of the `input` element.
  static member inline name (value: string) = Interop.mkAttr "name" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// Callback fired when the error associated to the current value changes.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: Event -> unit) = Interop.mkAttr "onError" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// It prevents the user from changing the value of the field (not from interacting with the field).
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// The date used to generate a part of the new value that is not present in the format when both `value` and `defaultValue` are empty. For example, on time fields it will be used to determine the date to set.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// If `true`, the label is displayed as required and the `input` element is required.
  static member inline required (value: bool) = Interop.mkAttr "required" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableDate" value
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableMonth" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableTime" value
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableYear" value
  /// If `true`, the format will respect the leading zeroes (e.g: on dayjs, the format `M/D/YYYY` will render `8/16/2018`) If `false`, the format will always add leading zeroes (e.g: on dayjs, the format `M/D/YYYY` will render `08/16/2018`)
  ///
  /// Warning n°1: Luxon is not able to respect the leading zeroes when using macro tokens (e.g: "DD"), so `shouldRespectLeadingZeros={true}` might lead to inconsistencies when using `AdapterLuxon`.
  ///
  /// Warning n°2: When `shouldRespectLeadingZeros={true}`, the field will add an invisible character on the sections containing a single digit to make sure `onChange` is fired. If you need to get the clean value from the input, you can remove this character using `input.value.replace(/\u200e/g, '')`.
  ///
  /// Warning n°3: When used in strict mode, dayjs and moment require to respect the leading zeros. This mean that when using `shouldRespectLeadingZeros={false}`, if you retrieve the value directly from the input (not listening to `onChange`) and your format contains tokens without leading zeros, the value will not be parsed by your library.
  static member inline shouldRespectLeadingZeros (value: bool) = Interop.mkAttr "shouldRespectLeadingZeros" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The ref object used to imperatively interact with the field.
  static member inline unstableFieldRef (value: Func<obj, obj>) = Interop.mkAttr "unstableFieldRef" value
  /// The ref object used to imperatively interact with the field.
  static member inline unstableFieldRef (value: obj) = Interop.mkAttr "unstableFieldRef" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module dateTimeField =

  /// The color of the component. It supports both default and custom theme colors, which can be added as shown in the [palette customization guide](https://mui.com/material-ui/customization/palette/#adding-new-colors).
  [<Erase>]
  type color =
    static member inline error = Interop.mkAttr "color" "error"
    static member inline info = Interop.mkAttr "color" "info"
    static member inline primary = Interop.mkAttr "color" "primary"
    static member inline secondary = Interop.mkAttr "color" "secondary"
    static member inline success = Interop.mkAttr "color" "success"
    static member inline warning = Interop.mkAttr "color" "warning"

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// If `dense` or `normal`, will adjust vertical spacing of this and contained components.
  [<Erase>]
  type margin =
    static member inline dense = Interop.mkAttr "margin" "dense"
    static member inline none = Interop.mkAttr "margin" "none"
    static member inline normal = Interop.mkAttr "margin" "normal"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The size of the component.
  [<Erase>]
  type size =
    static member inline medium = Interop.mkAttr "size" "medium"
    static member inline small = Interop.mkAttr "size" "small"

  /// The variant to use.
  [<Erase>]
  type variant =
    static member inline filled = Interop.mkAttr "variant" "filled"
    static member inline outlined = Interop.mkAttr "variant" "outlined"
    static member inline standard = Interop.mkAttr "variant" "standard"


[<Erase>]
type dateTimePicker =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (formatter: string -> string) = Interop.mkAttr "dayOfWeekFormatter" (Func<_, _> formatter)
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// CSS media query when `Mobile` mode will be changed to `Desktop`.
  static member inline desktopModeMediaQuery (value: string) = Interop.mkAttr "desktopModeMediaQuery" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Maximal selectable moment of time with binding to date, to set max time in each day use `maxTime`.
  static member inline maxDateTime (value: 'T) = Interop.mkAttr "maxDateTime" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Minimal selectable moment of time with binding to date, to set min time in each day use `minTime`.
  static member inline minDateTime (value: 'T) = Interop.mkAttr "minDateTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> JS.Promise<unit>) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: CalendarOrClockPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: 'TDate -> unit) = Interop.mkAttr "onYearChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (shouldDisableDate: System.DateTime -> bool) = Interop.mkAttr "shouldDisableDate" (Func<_, _> shouldDisableDate)
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (shouldDisableMonth: int -> bool) = Interop.mkAttr "shouldDisableMonth" (Func<_, _> shouldDisableMonth)
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (shouldDisableTime: System.DateTime -> string -> bool) = Interop.mkAttr "shouldDisableTime" (Func<_, _, _> shouldDisableTime)
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (shouldDisableYear: int -> bool) = Interop.mkAttr "shouldDisableYear" (Func<_, _> shouldDisableYear)
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// If `true`, disabled digital clock items will not be rendered.
  static member inline skipDisabled (value: bool) = Interop.mkAttr "skipDisabled" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// The time steps between two time unit options. For example, if `timeStep.minutes = 8`, then the available minute options will be `[0, 8, 16, 24, 32, 40, 48, 56]`. When single column time renderer is used, only `timeStep.minutes` will be used.
  static member inline timeSteps (?hours: float, ?minutes: float, ?seconds: float) = Interop.mkAttr "timeSteps" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?day: Func<obj, obj>, ?hours: Func<obj, obj>, ?meridiem: Func<obj, obj>, ?minutes: Func<obj, obj>, ?month: Func<obj, obj>, ?seconds: Func<obj, obj>, ?year: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if day.IsSome then x?``day`` <- day.Value); (if hours.IsSome then x?``hours`` <- hours.Value); (if meridiem.IsSome then x?``meridiem`` <- meridiem.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if month.IsSome then x?``month`` <- month.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); (if year.IsSome then x?``year`` <- year.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: DateTimePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module dateTimePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline meridiem = Interop.mkAttr "openTo" "meridiem"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline meridiem = Interop.mkAttr "view" "meridiem"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline seconds = Interop.mkAttr "view" "seconds"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type dateTimePickerTabs =
  /// Callback called when a tab is clicked
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The view to open
  static member inline onViewChange (handler: Event -> unit) = Interop.mkAttr "onViewChange" handler
  /// Date tab icon.
  static member inline dateIcon (element: ReactElement) = Interop.mkAttr "dateIcon" element
  /// Toggles visibility of the tabs allowing view switching.
  static member inline hidden (value: bool) = Interop.mkAttr "hidden" value
  /// Time tab icon.
  static member inline timeIcon (element: ReactElement) = Interop.mkAttr "timeIcon" element
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module dateTimePickerTabs =

  /// Currently visible picker view.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline meridiem = Interop.mkAttr "view" "meridiem"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline seconds = Interop.mkAttr "view" "seconds"
    static member inline year = Interop.mkAttr "view" "year"


[<Erase>]
type dateTimePickerToolbar =
  /// Callback called when a toolbar is clicked
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The view to open
  static member inline onViewChange (handler: Event -> unit) = Interop.mkAttr "onViewChange" handler
  /// className applied to the root component.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, show the toolbar even in desktop mode.
  static member inline hidden (value: bool) = Interop.mkAttr "hidden" value
  /// Toolbar date format.
  static member inline toolbarFormat (value: string) = Interop.mkAttr "toolbarFormat" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: ReactElement) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (values: seq<ReactElement>) = Interop.mkAttr "toolbarPlaceholder" values
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: string) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (values: string seq) = Interop.mkAttr "toolbarPlaceholder" values
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: int) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: float) = Interop.mkAttr "toolbarPlaceholder" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module dateTimePickerToolbar =

  /// Currently visible picker view.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline meridiem = Interop.mkAttr "view" "meridiem"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline seconds = Interop.mkAttr "view" "seconds"
    static member inline year = Interop.mkAttr "view" "year"


[<Erase>]
type dayCalendarSkeleton =
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()


[<Erase>]
type desktopDatePicker =
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (formatter: string -> string) = Interop.mkAttr "dayOfWeekFormatter" (Func<_, _> formatter)
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> JS.Promise<unit>) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: CalendarPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: 'TDate -> unit) = Interop.mkAttr "onYearChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (shouldDisableDate: System.DateTime -> bool) = Interop.mkAttr "shouldDisableDate" (Func<_, _> shouldDisableDate)
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (shouldDisableMonth: int -> bool) = Interop.mkAttr "shouldDisableMonth" (Func<_, _> shouldDisableMonth)
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (shouldDisableYear: int -> bool) = Interop.mkAttr "shouldDisableYear" (Func<_, _> shouldDisableYear)
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?day: Func<obj, obj>, ?month: Func<obj, obj>, ?year: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if day.IsSome then x?``day`` <- day.Value); (if month.IsSome then x?``month`` <- month.Value); (if year.IsSome then x?``year`` <- year.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: DatePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module desktopDatePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type desktopDateTimePicker =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (formatter: string -> string) = Interop.mkAttr "dayOfWeekFormatter" (Func<_, _> formatter)
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Maximal selectable moment of time with binding to date, to set max time in each day use `maxTime`.
  static member inline maxDateTime (value: 'T) = Interop.mkAttr "maxDateTime" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Minimal selectable moment of time with binding to date, to set min time in each day use `minTime`.
  static member inline minDateTime (value: 'T) = Interop.mkAttr "minDateTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> JS.Promise<unit>) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: CalendarOrClockPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: 'TDate -> unit) = Interop.mkAttr "onYearChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (shouldDisableDate: System.DateTime -> bool) = Interop.mkAttr "shouldDisableDate" (Func<_, _> shouldDisableDate)
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (shouldDisableMonth: int -> bool) = Interop.mkAttr "shouldDisableMonth" (Func<_, _> shouldDisableMonth)
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (shouldDisableTime: System.DateTime -> string -> bool) = Interop.mkAttr "shouldDisableTime" (Func<_, _, _> shouldDisableTime)
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (shouldDisableYear: int -> bool) = Interop.mkAttr "shouldDisableYear" (Func<_, _> shouldDisableYear)
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// If `true`, disabled digital clock items will not be rendered.
  static member inline skipDisabled (value: bool) = Interop.mkAttr "skipDisabled" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// The time steps between two time unit options. For example, if `timeStep.minutes = 8`, then the available minute options will be `[0, 8, 16, 24, 32, 40, 48, 56]`. When single column time renderer is used, only `timeStep.minutes` will be used.
  static member inline timeSteps (?hours: float, ?minutes: float, ?seconds: float) = Interop.mkAttr "timeSteps" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?day: Func<obj, obj>, ?hours: Func<obj, obj>, ?meridiem: Func<obj, obj>, ?minutes: Func<obj, obj>, ?month: Func<obj, obj>, ?seconds: Func<obj, obj>, ?year: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if day.IsSome then x?``day`` <- day.Value); (if hours.IsSome then x?``hours`` <- hours.Value); (if meridiem.IsSome then x?``meridiem`` <- meridiem.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if month.IsSome then x?``month`` <- month.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); (if year.IsSome then x?``year`` <- year.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: DateTimePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module desktopDateTimePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline meridiem = Interop.mkAttr "openTo" "meridiem"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline meridiem = Interop.mkAttr "view" "meridiem"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline seconds = Interop.mkAttr "view" "seconds"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type desktopTimePicker =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: ClockPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (shouldDisableTime: System.DateTime -> string -> bool) = Interop.mkAttr "shouldDisableTime" (Func<_, _, _> shouldDisableTime)
  /// If `true`, disabled digital clock items will not be rendered.
  static member inline skipDisabled (value: bool) = Interop.mkAttr "skipDisabled" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Amount of time options below or at which the single column time renderer is used.
  static member inline thresholdToRenderTimeInASingleColumn (value: int) = Interop.mkAttr "thresholdToRenderTimeInASingleColumn" value
  /// Amount of time options below or at which the single column time renderer is used.
  static member inline thresholdToRenderTimeInASingleColumn (value: float) = Interop.mkAttr "thresholdToRenderTimeInASingleColumn" value
  /// The time steps between two time unit options. For example, if `timeStep.minutes = 8`, then the available minute options will be `[0, 8, 16, 24, 32, 40, 48, 56]`. When single column time renderer is used, only `timeStep.minutes` will be used.
  static member inline timeSteps (?hours: float, ?minutes: float, ?seconds: float) = Interop.mkAttr "timeSteps" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?hours: Func<obj, obj>, ?meridiem: Func<obj, obj>, ?minutes: Func<obj, obj>, ?seconds: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if meridiem.IsSome then x?``meridiem`` <- meridiem.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: TimePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module desktopTimePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline meridiem = Interop.mkAttr "openTo" "meridiem"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline meridiem = Interop.mkAttr "view" "meridiem"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline seconds = Interop.mkAttr "view" "seconds"


[<Erase>]
type digitalClock =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overrideable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default selected value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker views and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate | null, selectionState: PickerSelectionState | undefined, selectedView: TView | undefined) => void`
  ///
  /// *value:* The new value.
  ///
  /// *selectionState:* Indicates if the date selection is complete.
  ///
  /// *selectedView:* Indicates the view in which the selection has been made.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// Callback fired on focused view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView, hasFocus: boolean) => void`
  ///
  /// *view:* The new view to focus or not.
  ///
  /// *hasFocus:*`true` if the view should be focused.
  static member inline onFocusedViewChange (handler: Event -> unit) = Interop.mkAttr "onFocusedViewChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: Event -> unit) = Interop.mkAttr "onViewChange" handler
  /// If `true`, the picker views and text field are read-only.
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// The date used to generate the new value when both `value` and `defaultValue` are empty.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableTime" value
  /// If `true`, disabled digital clock items will not be rendered.
  static member inline skipDisabled (value: bool) = Interop.mkAttr "skipDisabled" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overrideable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// The time steps between two time options. For example, if `timeStep = 45`, then the available time options will be `[00:00, 00:45, 01:30, 02:15, 03:00, etc.]`.
  static member inline timeStep (value: int) = Interop.mkAttr "timeStep" value
  /// The time steps between two time options. For example, if `timeStep = 45`, then the available time options will be `[00:00, 00:45, 01:30, 02:15, 03:00, etc.]`.
  static member inline timeStep (value: float) = Interop.mkAttr "timeStep" value
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Available views.
  static member inline views ([<ParamArray>] values: string []) = Interop.mkAttr "views" values
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module digitalClock =

  /// Controlled focused view.
  [<Erase>]
  type focusedView =
    static member inline hours = Interop.mkAttr "focusedView" "hours"

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline hours = Interop.mkAttr "openTo" "hours"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline hours = Interop.mkAttr "view" "hours"


[<Erase>]
type localizationProvider =
  /// Locale for the date library you are using
  static member inline adapterLocale (value: 'T) = Interop.mkAttr "adapterLocale" value
  /// Formats that are used for any child pickers
  static member inline dateFormats (?dayOfMonth: string, ?fullDate: string, ?fullDateTime: string, ?fullDateTime12h: string, ?fullDateTime24h: string, ?fullDateWithWeekday: string, ?fullTime: string, ?fullTime12h: string, ?fullTime24h: string, ?hours12h: string, ?hours24h: string, ?keyboardDate: string, ?keyboardDateTime: string, ?keyboardDateTime12h: string, ?keyboardDateTime24h: string, ?meridiem: string, ?minutes: string, ?month: string, ?monthAndDate: string, ?monthAndYear: string, ?monthShort: string, ?normalDate: string, ?normalDateWithWeekday: string, ?seconds: string, ?shortDate: string, ?weekday: string, ?weekdayShort: string, ?year: string) = Interop.mkAttr "dateFormats" (let x = createEmpty<obj> in (if dayOfMonth.IsSome then x?``dayOfMonth`` <- dayOfMonth); (if fullDate.IsSome then x?``fullDate`` <- fullDate); (if fullDateTime.IsSome then x?``fullDateTime`` <- fullDateTime); (if fullDateTime12h.IsSome then x?``fullDateTime12h`` <- fullDateTime12h); (if fullDateTime24h.IsSome then x?``fullDateTime24h`` <- fullDateTime24h); (if fullDateWithWeekday.IsSome then x?``fullDateWithWeekday`` <- fullDateWithWeekday); (if fullTime.IsSome then x?``fullTime`` <- fullTime); (if fullTime12h.IsSome then x?``fullTime12h`` <- fullTime12h); (if fullTime24h.IsSome then x?``fullTime24h`` <- fullTime24h); (if hours12h.IsSome then x?``hours12h`` <- hours12h); (if hours24h.IsSome then x?``hours24h`` <- hours24h); (if keyboardDate.IsSome then x?``keyboardDate`` <- keyboardDate); (if keyboardDateTime.IsSome then x?``keyboardDateTime`` <- keyboardDateTime); (if keyboardDateTime12h.IsSome then x?``keyboardDateTime12h`` <- keyboardDateTime12h); (if keyboardDateTime24h.IsSome then x?``keyboardDateTime24h`` <- keyboardDateTime24h); (if meridiem.IsSome then x?``meridiem`` <- meridiem); (if minutes.IsSome then x?``minutes`` <- minutes); (if month.IsSome then x?``month`` <- month); (if monthAndDate.IsSome then x?``monthAndDate`` <- monthAndDate); (if monthAndYear.IsSome then x?``monthAndYear`` <- monthAndYear); (if monthShort.IsSome then x?``monthShort`` <- monthShort); (if normalDate.IsSome then x?``normalDate`` <- normalDate); (if normalDateWithWeekday.IsSome then x?``normalDateWithWeekday`` <- normalDateWithWeekday); (if seconds.IsSome then x?``seconds`` <- seconds); (if shortDate.IsSome then x?``shortDate`` <- shortDate); (if weekday.IsSome then x?``weekday`` <- weekday); (if weekdayShort.IsSome then x?``weekdayShort`` <- weekdayShort); (if year.IsSome then x?``year`` <- year); x)
  /// Date library instance you are using, if it has some global overrides `jsx dateLibInstance={momentTimeZone} `
  static member inline dateLibInstance (value: obj) = Interop.mkAttr "dateLibInstance" value
  /// Locale for components texts
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module localizationProvider =

  /// Date library adapter class function. See the localization provider [date adapter setup section](https://mui.com/x/react-date-pickers/getting-started/#setup-your-date-library-adapter) for more details.
  [<Erase>]
  type dateAdapter =
    static member inline dayJs = Interop.mkAttr "dateAdapter" (import "AdapterDayjs" "@mui/x-date-pickers/AdapterDayjs")
    static member inline dateFns = Interop.mkAttr "dateAdapter" (import "AdapterDateFns" "@mui/x-date-pickers/AdapterDateFns")
    static member inline luxon = Interop.mkAttr "dateAdapter" (import "AdapterLuxon" "@mui/x-date-pickers/AdapterLuxon")
    static member inline moment = Interop.mkAttr "dateAdapter" (import "AdapterMoment" "@mui/x-date-pickers/AdapterMoment")


[<Erase>]
type mobileDatePicker =
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (formatter: string -> string) = Interop.mkAttr "dayOfWeekFormatter" (Func<_, _> formatter)
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> JS.Promise<unit>) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: CalendarPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: 'TDate -> unit) = Interop.mkAttr "onYearChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (shouldDisableDate: System.DateTime -> bool) = Interop.mkAttr "shouldDisableDate" (Func<_, _> shouldDisableDate)
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (shouldDisableMonth: int -> bool) = Interop.mkAttr "shouldDisableMonth" (Func<_, _> shouldDisableMonth)
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (shouldDisableYear: int -> bool) = Interop.mkAttr "shouldDisableYear" (Func<_, _> shouldDisableYear)
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?day: Func<obj, obj>, ?month: Func<obj, obj>, ?year: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if day.IsSome then x?``day`` <- day.Value); (if month.IsSome then x?``month`` <- month.Value); (if year.IsSome then x?``year`` <- year.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: DatePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module mobileDatePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type mobileDateTimePicker =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (formatter: string -> string) = Interop.mkAttr "dayOfWeekFormatter" (Func<_, _> formatter)
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Maximal selectable moment of time with binding to date, to set max time in each day use `maxTime`.
  static member inline maxDateTime (value: 'T) = Interop.mkAttr "maxDateTime" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Minimal selectable moment of time with binding to date, to set min time in each day use `minTime`.
  static member inline minDateTime (value: 'T) = Interop.mkAttr "minDateTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> JS.Promise<unit>) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: CalendarOrClockPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: 'TDate -> unit) = Interop.mkAttr "onYearChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (shouldDisableDate: System.DateTime -> bool) = Interop.mkAttr "shouldDisableDate" (Func<_, _> shouldDisableDate)
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (shouldDisableMonth: int -> bool) = Interop.mkAttr "shouldDisableMonth" (Func<_, _> shouldDisableMonth)
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (shouldDisableTime: System.DateTime -> string -> bool) = Interop.mkAttr "shouldDisableTime" (Func<_, _, _> shouldDisableTime)
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (shouldDisableYear: int -> bool) = Interop.mkAttr "shouldDisableYear" (Func<_, _> shouldDisableYear)
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?day: Func<obj, obj>, ?hours: Func<obj, obj>, ?minutes: Func<obj, obj>, ?month: Func<obj, obj>, ?seconds: Func<obj, obj>, ?year: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if day.IsSome then x?``day`` <- day.Value); (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if month.IsSome then x?``month`` <- month.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); (if year.IsSome then x?``year`` <- year.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: DateTimePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module mobileDateTimePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline seconds = Interop.mkAttr "view" "seconds"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type mobileTimePicker =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: ClockPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (shouldDisableTime: System.DateTime -> string -> bool) = Interop.mkAttr "shouldDisableTime" (Func<_, _, _> shouldDisableTime)
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?hours: Func<obj, obj>, ?minutes: Func<obj, obj>, ?seconds: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: TimePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module mobileTimePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline seconds = Interop.mkAttr "view" "seconds"


[<Erase>]
type monthCalendar =
  /// className applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// The default selected value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true` picker is disabled
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate) => void`
  ///
  /// *value:* The new value.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// If `true` picker is readonly
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// The date used to generate the new value when both `value` and `defaultValue` are empty.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableMonth" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module monthCalendar =

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4


[<Erase>]
type multiSectionDigitalClock =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overrideable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default selected value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker views and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate | null, selectionState: PickerSelectionState | undefined, selectedView: TView | undefined) => void`
  ///
  /// *value:* The new value.
  ///
  /// *selectionState:* Indicates if the date selection is complete.
  ///
  /// *selectedView:* Indicates the view in which the selection has been made.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// Callback fired on focused view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView, hasFocus: boolean) => void`
  ///
  /// *view:* The new view to focus or not.
  ///
  /// *hasFocus:*`true` if the view should be focused.
  static member inline onFocusedViewChange (handler: Event -> unit) = Interop.mkAttr "onFocusedViewChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: Event -> unit) = Interop.mkAttr "onViewChange" handler
  /// If `true`, the picker views and text field are read-only.
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// The date used to generate the new value when both `value` and `defaultValue` are empty.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableTime" value
  /// If `true`, disabled digital clock items will not be rendered.
  static member inline skipDisabled (value: bool) = Interop.mkAttr "skipDisabled" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overrideable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// The time steps between two time unit options. For example, if `timeStep.minutes = 8`, then the available minute options will be `[0, 8, 16, 24, 32, 40, 48, 56]`.
  static member inline timeSteps (?hours: float, ?minutes: float, ?seconds: float) = Interop.mkAttr "timeSteps" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Available views.
  static member inline views ([<ParamArray>] values: string []) = Interop.mkAttr "views" values
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module multiSectionDigitalClock =

  /// Controlled focused view.
  [<Erase>]
  type focusedView =
    static member inline hours = Interop.mkAttr "focusedView" "hours"
    static member inline meridiem = Interop.mkAttr "focusedView" "meridiem"
    static member inline minutes = Interop.mkAttr "focusedView" "minutes"
    static member inline seconds = Interop.mkAttr "focusedView" "seconds"

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline meridiem = Interop.mkAttr "openTo" "meridiem"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline meridiem = Interop.mkAttr "view" "meridiem"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline seconds = Interop.mkAttr "view" "seconds"


[<Erase>]
type pickersActionBar =
  /// Ordered array of actions to display. If empty, does not display that action bar.
  static member inline actions ([<ParamArray>] values: string []) = Interop.mkAttr "actions" values
  /// If `true`, the actions do not have additional margin.
  static member inline disableSpacing (value: bool) = Interop.mkAttr "disableSpacing" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()


[<Erase>]
type pickersDay =
  /// The date to show.
  static member inline day (value: 'T) = Interop.mkAttr "day" value
  /// If `true`, day is the first visible cell of the month. Either the first day of the month or the first day of the week depending on `showDaysOutsideCurrentMonth`.
  static member inline isFirstVisibleCell (value: bool) = Interop.mkAttr "isFirstVisibleCell" value
  /// If `true`, day is the last visible cell of the month. Either the last day of the month or the last day of the week depending on `showDaysOutsideCurrentMonth`.
  static member inline isLastVisibleCell (value: bool) = Interop.mkAttr "isLastVisibleCell" value
  /// If `true`, day is outside of month and will be hidden.
  static member inline outsideCurrentMonth (value: bool) = Interop.mkAttr "outsideCurrentMonth" value
  /// A ref for imperative actions. It currently only supports `focusVisible()` action.
  static member inline action (value: Func<obj, obj>) = Interop.mkAttr "action" value
  /// A ref for imperative actions. It currently only supports `focusVisible()` action.
  static member inline action (?current: {| focusVisible: Func<obj, obj> |}) = Interop.mkAttr "action" (let x = createEmpty<obj> in (if current.IsSome then x?``current`` <- current.Value); x)
  /// If `true`, the ripples are centered. They won't start at the cursor interaction position.
  static member inline centerRipple (value: bool) = Interop.mkAttr "centerRipple" value
  /// If `true`, renders as disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// If `true`, days are rendering without margin. Useful for displaying linked range of days.
  static member inline disableMargin (value: bool) = Interop.mkAttr "disableMargin" value
  /// If `true`, the ripple effect is disabled.
  ///
  /// ⚠️ Without a ripple there is no styling for :focus-visible by default. Be sure to highlight the element by applying separate styles with the `.Mui-focusVisible` class.
  static member inline disableRipple (value: bool) = Interop.mkAttr "disableRipple" value
  /// If `true`, the touch ripple effect is disabled.
  static member inline disableTouchRipple (value: bool) = Interop.mkAttr "disableTouchRipple" value
  /// If `true`, the base button will have a keyboard focus ripple.
  static member inline focusRipple (value: bool) = Interop.mkAttr "focusRipple" value
  /// This prop can help identify which element has keyboard focus. The class name will be applied when the element gains the focus through keyboard interaction. It's a polyfill for the [CSS :focus-visible selector](https://drafts.csswg.org/selectors-4/#the-focus-visible-pseudo). The rationale for using this feature [is explained here](https://github.com/WICG/focus-visible/blob/HEAD/explainer.md). A [polyfill can be used](https://github.com/WICG/focus-visible) to apply a `focus-visible` class to other components if needed.
  static member inline focusVisibleClassName (value: string) = Interop.mkAttr "focusVisibleClassName" value
  /// Callback fired when the component is focused with a keyboard. We trigger a `onFocus` callback too.
  static member inline onFocusVisible (handler: Event -> unit) = Interop.mkAttr "onFocusVisible" handler
  /// If `true`, renders as selected.
  static member inline selected (value: bool) = Interop.mkAttr "selected" value
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// If `true`, renders as today date.
  static member inline today (value: bool) = Interop.mkAttr "today" value
  /// Props applied to the `TouchRipple` element.
  static member inline TouchRippleProps (props: IReactProperty list) = Interop.mkAttr "TouchRippleProps" (createObj !!props)
  /// A ref that points to the `TouchRipple` element.
  static member inline touchRippleRef (value: Func<obj, obj>) = Interop.mkAttr "touchRippleRef" value
  /// A ref that points to the `TouchRipple` element.
  static member inline touchRippleRef (?current: {| pulsate: Func<obj, obj>; start: Func<obj, obj>; stop: Func<obj, obj> |}) = Interop.mkAttr "touchRippleRef" (let x = createEmpty<obj> in (if current.IsSome then x?``current`` <- current.Value); x)
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()


[<Erase>]
type pickersLayout =
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module pickersLayout =

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"


[<Erase>]
type pickersShortcuts =
  /// If `true`, compact vertical padding designed for keyboard and mouse input is used for the list and list items. The prop is available to descendant components as the `dense` context.
  static member inline dense (value: bool) = Interop.mkAttr "dense" value
  /// If `true`, vertical padding is removed from the list.
  static member inline disablePadding (value: bool) = Interop.mkAttr "disablePadding" value
  /// Ordered array of shortcuts to display. If empty, does not display the shortcuts.
  static member inline items ([<ParamArray>] values: {| getValue: Func<obj, obj>; label: string |} []) = Interop.mkAttr "items" values
  /// The content of the subheader, normally `ListSubheader`.
  static member inline subheader (value: ReactElement) = Interop.mkAttr "subheader" value
  /// The content of the subheader, normally `ListSubheader`.
  static member inline subheader (values: seq<ReactElement>) = Interop.mkAttr "subheader" values
  /// The content of the subheader, normally `ListSubheader`.
  static member inline subheader (value: string) = Interop.mkAttr "subheader" value
  /// The content of the subheader, normally `ListSubheader`.
  static member inline subheader (values: string seq) = Interop.mkAttr "subheader" values
  /// The content of the subheader, normally `ListSubheader`.
  static member inline subheader (value: int) = Interop.mkAttr "subheader" value
  /// The content of the subheader, normally `ListSubheader`.
  static member inline subheader (value: float) = Interop.mkAttr "subheader" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module pickersShortcuts =

  /// Importance of the change when picking a shortcut: - "accept": fires `onChange`, fires `onAccept` and closes the picker. - "set": fires `onChange` but do not fire `onAccept` and does not close the picker.
  [<Erase>]
  type changeImportance =
    static member inline accept = Interop.mkAttr "changeImportance" "accept"
    static member inline set = Interop.mkAttr "changeImportance" "set"


[<Erase>]
type staticDatePicker =
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (formatter: string -> string) = Interop.mkAttr "dayOfWeekFormatter" (Func<_, _> formatter)
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** - Please avoid using as it will be removed in next major version.
  ///
  ///   Callback fired when component requests to be closed. Can be fired when selecting (by default on `desktop` mode) or clearing a value.
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> JS.Promise<unit>) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: CalendarPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: 'TDate -> unit) = Interop.mkAttr "onYearChange" handler
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (shouldDisableDate: System.DateTime -> bool) = Interop.mkAttr "shouldDisableDate" (Func<_, _> shouldDisableDate)
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (shouldDisableMonth: int -> bool) = Interop.mkAttr "shouldDisableMonth" (Func<_, _> shouldDisableMonth)
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (shouldDisableYear: int -> bool) = Interop.mkAttr "shouldDisableYear" (Func<_, _> shouldDisableYear)
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?day: Func<obj, obj>, ?month: Func<obj, obj>, ?year: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if day.IsSome then x?``day`` <- day.Value); (if month.IsSome then x?``month`` <- month.Value); (if year.IsSome then x?``year`` <- year.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: DatePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module staticDatePicker =

  /// Force static wrapper inner components to be rendered in mobile or desktop mode.
  [<Erase>]
  type displayStaticWrapperAs =
    static member inline desktop = Interop.mkAttr "displayStaticWrapperAs" "desktop"
    static member inline mobile = Interop.mkAttr "displayStaticWrapperAs" "mobile"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type staticDateTimePicker =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// Formats the day of week displayed in the calendar header.
  ///
  /// **Signature:**
  ///
  /// `function(day: string) => string`
  ///
  /// *day:* The day of week provided by the adapter's method `getWeekdays`.
  ///
  /// *returns* (string): The name to display.
  static member inline dayOfWeekFormatter (formatter: string -> string) = Interop.mkAttr "dayOfWeekFormatter" (Func<_, _> formatter)
  /// Default calendar month displayed when `value` and `defaultValue` are empty.
  static member inline defaultCalendarMonth (value: 'T) = Interop.mkAttr "defaultCalendarMonth" value
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the week number will be display in the calendar.
  static member inline displayWeekNumber (value: bool) = Interop.mkAttr "displayWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: int) = Interop.mkAttr "fixedWeekNumber" value
  /// Calendar will show more weeks in order to match this value. Put it to 6 for having fix number of week in Gregorian calendars
  static member inline fixedWeekNumber (value: float) = Interop.mkAttr "fixedWeekNumber" value
  /// If `true`, calls `renderLoading` instead of rendering the day calendar. Can be used to preload information and show it in calendar.
  static member inline loading (value: bool) = Interop.mkAttr "loading" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Maximal selectable moment of time with binding to date, to set max time in each day use `maxTime`.
  static member inline maxDateTime (value: 'T) = Interop.mkAttr "maxDateTime" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Minimal selectable moment of time with binding to date, to set min time in each day use `minTime`.
  static member inline minDateTime (value: 'T) = Interop.mkAttr "minDateTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** - Please avoid using as it will be removed in next major version.
  ///
  ///   Callback fired when component requests to be closed. Can be fired when selecting (by default on `desktop` mode) or clearing a value.
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> unit) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on month change.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => void`
  ///
  /// *month:* The new month.
  static member inline onMonthChange (handler: 'TDate -> JS.Promise<unit>) = Interop.mkAttr "onMonthChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: CalendarOrClockPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Callback fired on year change.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => void`
  ///
  /// *year:* The new year.
  static member inline onYearChange (handler: 'TDate -> unit) = Interop.mkAttr "onYearChange" handler
  /// Disable heavy animations.
  static member inline reduceAnimations (value: bool) = Interop.mkAttr "reduceAnimations" value
  /// Component displaying when passed `loading` true.
  ///
  /// **Signature:**
  ///
  /// `function() => React.ReactNode`
  ///
  /// *returns* (React.ReactNode): The node to render when loading.
  static member inline renderLoading (renderer: unit -> ReactElement) = Interop.mkAttr "renderLoading" renderer
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific date.
  ///
  /// **Signature:**
  ///
  /// `function(day: TDate) => boolean`
  ///
  /// *day:* The date to test.
  ///
  /// *returns* (boolean): If `true` the date will be disabled.
  static member inline shouldDisableDate (shouldDisableDate: System.DateTime -> bool) = Interop.mkAttr "shouldDisableDate" (Func<_, _> shouldDisableDate)
  /// Disable specific month.
  ///
  /// **Signature:**
  ///
  /// `function(month: TDate) => boolean`
  ///
  /// *month:* The month to test.
  ///
  /// *returns* (boolean): If `true`, the month will be disabled.
  static member inline shouldDisableMonth (shouldDisableMonth: int -> bool) = Interop.mkAttr "shouldDisableMonth" (Func<_, _> shouldDisableMonth)
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (shouldDisableTime: System.DateTime -> string -> bool) = Interop.mkAttr "shouldDisableTime" (Func<_, _, _> shouldDisableTime)
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (shouldDisableYear: int -> bool) = Interop.mkAttr "shouldDisableYear" (Func<_, _> shouldDisableYear)
  /// If `true`, days outside the current month are rendered:
  ///
  /// - if `fixedWeekNumber` is defined, renders days to have the weeks requested.
  ///
  /// - if `fixedWeekNumber` is not defined, renders day to fill the first and last week of the current month.
  ///
  /// - ignored if `calendars` equals more than `1` on range pickers.
  static member inline showDaysOutsideCurrentMonth (value: bool) = Interop.mkAttr "showDaysOutsideCurrentMonth" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?day: Func<obj, obj>, ?hours: Func<obj, obj>, ?minutes: Func<obj, obj>, ?month: Func<obj, obj>, ?seconds: Func<obj, obj>, ?year: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if day.IsSome then x?``day`` <- day.Value); (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if month.IsSome then x?``month`` <- month.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); (if year.IsSome then x?``year`` <- year.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: DateTimePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module staticDateTimePicker =

  /// Force static wrapper inner components to be rendered in mobile or desktop mode.
  [<Erase>]
  type displayStaticWrapperAs =
    static member inline desktop = Interop.mkAttr "displayStaticWrapperAs" "desktop"
    static member inline mobile = Interop.mkAttr "displayStaticWrapperAs" "mobile"

  /// Months rendered per row.
  [<Erase>]
  type monthsPerRow =
    static member inline _3 = Interop.mkAttr "monthsPerRow" 3
    static member inline _4 = Interop.mkAttr "monthsPerRow" 4

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline day = Interop.mkAttr "openTo" "day"
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline month = Interop.mkAttr "openTo" "month"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"
    static member inline year = Interop.mkAttr "openTo" "year"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline day = Interop.mkAttr "view" "day"
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline month = Interop.mkAttr "view" "month"
    static member inline seconds = Interop.mkAttr "view" "seconds"
    static member inline year = Interop.mkAttr "view" "year"

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4


[<Erase>]
type staticTimePicker =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** - Please avoid using as it will be removed in next major version.
  ///
  ///   Callback fired when component requests to be closed. Can be fired when selecting (by default on `desktop` mode) or clearing a value.
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: ClockPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (shouldDisableTime: System.DateTime -> string -> bool) = Interop.mkAttr "shouldDisableTime" (Func<_, _, _> shouldDisableTime)
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?hours: Func<obj, obj>, ?minutes: Func<obj, obj>, ?seconds: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: TimePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module staticTimePicker =

  /// Force static wrapper inner components to be rendered in mobile or desktop mode.
  [<Erase>]
  type displayStaticWrapperAs =
    static member inline desktop = Interop.mkAttr "displayStaticWrapperAs" "desktop"
    static member inline mobile = Interop.mkAttr "displayStaticWrapperAs" "mobile"

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline seconds = Interop.mkAttr "view" "seconds"


[<Erase>]
type timeClock =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default selected value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the picker views and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate | null, selectionState: PickerSelectionState | undefined, selectedView: TView | undefined) => void`
  ///
  /// *value:* The new value.
  ///
  /// *selectionState:* Indicates if the date selection is complete.
  ///
  /// *selectedView:* Indicates the view in which the selection has been made.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// Callback fired on focused view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView, hasFocus: boolean) => void`
  ///
  /// *view:* The new view to focus or not.
  ///
  /// *hasFocus:*`true` if the view should be focused.
  static member inline onFocusedViewChange (handler: Event -> unit) = Interop.mkAttr "onFocusedViewChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: Event -> unit) = Interop.mkAttr "onViewChange" handler
  /// If `true`, the picker views and text field are read-only.
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// The date used to generate the new value when both `value` and `defaultValue` are empty.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableTime" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Available views.
  static member inline views ([<ParamArray>] values: string []) = Interop.mkAttr "views" values
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module timeClock =

  /// Controlled focused view.
  [<Erase>]
  type focusedView =
    static member inline hours = Interop.mkAttr "focusedView" "hours"
    static member inline minutes = Interop.mkAttr "focusedView" "minutes"
    static member inline seconds = Interop.mkAttr "focusedView" "seconds"

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline seconds = Interop.mkAttr "view" "seconds"


[<Erase>]
type timeField =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// If `true`, the `input` element is focused during the first mount.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default value. Use when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true`, the component is disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// If `true`, the component is displayed in focused state.
  static member inline focused (value: bool) = Interop.mkAttr "focused" value
  /// Format of the date when rendered in the input(s).
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Props applied to the
  ///
  ///   [`FormHelperText`](https://mui.com/material-ui/api/form-helper-text/) element.
  static member inline FormHelperTextProps (props: IReactProperty list) = Interop.mkAttr "FormHelperTextProps" (createObj !!props)
  /// If `true`, the input will take up the full width of its container.
  static member inline fullWidth (value: bool) = Interop.mkAttr "fullWidth" value
  /// The helper text content.
  static member inline helperText (value: ReactElement) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (values: seq<ReactElement>) = Interop.mkAttr "helperText" values
  /// The helper text content.
  static member inline helperText (value: string) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (values: string seq) = Interop.mkAttr "helperText" values
  /// The helper text content.
  static member inline helperText (value: int) = Interop.mkAttr "helperText" value
  /// The helper text content.
  static member inline helperText (value: float) = Interop.mkAttr "helperText" value
  /// If `true`, the label is hidden. This is used to increase density for a `FilledInput`. Be sure to add `aria-label` to the `input` element.
  static member inline hiddenLabel (value: bool) = Interop.mkAttr "hiddenLabel" value
  /// The id of the `input` element. Use this prop to make `label` and `helperText` accessible for screen readers.
  static member inline id (value: string) = Interop.mkAttr "id" value
  /// Props applied to the
  ///
  ///   [`InputLabel`](https://mui.com/material-ui/api/input-label/) element. Pointer events like `onClick` are enabled if and only if `shrink` is `true`.
  static member inline InputLabelProps (props: IReactProperty list) = Interop.mkAttr "InputLabelProps" (createObj !!props)
  /// [Attributes](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Attributes) applied to the `input` element.
  static member inline inputProps (props: IReactProperty list) = Interop.mkAttr "inputProps" (createObj !!props)
  /// Props applied to the Input element. It will be a
  ///
  ///   [`FilledInput`](https://mui.com/material-ui/api/filled-input/),
  ///
  ///   [`OutlinedInput`](https://mui.com/material-ui/api/outlined-input/) or
  ///
  ///   [`Input`](https://mui.com/material-ui/api/input/) component depending on the `variant` prop value.
  static member inline InputProps (props: IReactProperty list) = Interop.mkAttr "InputProps" (createObj !!props)
  /// Pass a ref to the `input` element.
  static member inline inputRef (value: Func<obj, obj>) = Interop.mkAttr "inputRef" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (current: 'T) = Interop.mkAttr "inputRef" (let x = createEmpty<obj> in x?``current`` <- current; x)
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Name attribute of the `input` element.
  static member inline name (value: string) = Interop.mkAttr "name" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// Callback fired when the error associated to the current value changes.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: Event -> unit) = Interop.mkAttr "onError" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// It prevents the user from changing the value of the field (not from interacting with the field).
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// The date used to generate a part of the new value that is not present in the format when both `value` and `defaultValue` are empty. For example, on time fields it will be used to determine the date to set.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// If `true`, the label is displayed as required and the `input` element is required.
  static member inline required (value: bool) = Interop.mkAttr "required" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableTime" value
  /// If `true`, the format will respect the leading zeroes (e.g: on dayjs, the format `M/D/YYYY` will render `8/16/2018`) If `false`, the format will always add leading zeroes (e.g: on dayjs, the format `M/D/YYYY` will render `08/16/2018`)
  ///
  /// Warning n°1: Luxon is not able to respect the leading zeroes when using macro tokens (e.g: "DD"), so `shouldRespectLeadingZeros={true}` might lead to inconsistencies when using `AdapterLuxon`.
  ///
  /// Warning n°2: When `shouldRespectLeadingZeros={true}`, the field will add an invisible character on the sections containing a single digit to make sure `onChange` is fired. If you need to get the clean value from the input, you can remove this character using `input.value.replace(/\u200e/g, '')`.
  ///
  /// Warning n°3: When used in strict mode, dayjs and moment require to respect the leading zeros. This mean that when using `shouldRespectLeadingZeros={false}`, if you retrieve the value directly from the input (not listening to `onChange`) and your format contains tokens without leading zeros, the value will not be parsed by your library.
  static member inline shouldRespectLeadingZeros (value: bool) = Interop.mkAttr "shouldRespectLeadingZeros" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The ref object used to imperatively interact with the field.
  static member inline unstableFieldRef (value: Func<obj, obj>) = Interop.mkAttr "unstableFieldRef" value
  /// The ref object used to imperatively interact with the field.
  static member inline unstableFieldRef (value: obj) = Interop.mkAttr "unstableFieldRef" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module timeField =

  /// The color of the component. It supports both default and custom theme colors, which can be added as shown in the [palette customization guide](https://mui.com/material-ui/customization/palette/#adding-new-colors).
  [<Erase>]
  type color =
    static member inline error = Interop.mkAttr "color" "error"
    static member inline info = Interop.mkAttr "color" "info"
    static member inline primary = Interop.mkAttr "color" "primary"
    static member inline secondary = Interop.mkAttr "color" "secondary"
    static member inline success = Interop.mkAttr "color" "success"
    static member inline warning = Interop.mkAttr "color" "warning"

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// If `dense` or `normal`, will adjust vertical spacing of this and contained components.
  [<Erase>]
  type margin =
    static member inline dense = Interop.mkAttr "margin" "dense"
    static member inline none = Interop.mkAttr "margin" "none"
    static member inline normal = Interop.mkAttr "margin" "normal"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The size of the component.
  [<Erase>]
  type size =
    static member inline medium = Interop.mkAttr "size" "medium"
    static member inline small = Interop.mkAttr "size" "small"

  /// The variant to use.
  [<Erase>]
  type variant =
    static member inline filled = Interop.mkAttr "variant" "filled"
    static member inline outlined = Interop.mkAttr "variant" "outlined"
    static member inline standard = Interop.mkAttr "variant" "standard"


[<Erase>]
type timePicker =
  /// 12h/24h view for hour selection clock.
  static member inline ampm (value: bool) = Interop.mkAttr "ampm" value
  /// Display ampm controls under the clock (instead of in the toolbar).
  static member inline ampmInClock (value: bool) = Interop.mkAttr "ampmInClock" value
  /// If `true`, the main element is focused during the first mount. This main element is: - the element chosen by the visible view if any (i.e: the selected day on the `day` view). - the `input` element if there is a field rendered.
  static member inline autoFocus (value: bool) = Interop.mkAttr "autoFocus" value
  /// Class name applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, the popover or modal will close after submitting the full date.
  static member inline closeOnSelect (value: bool) = Interop.mkAttr "closeOnSelect" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slots`.
  ///
  /// Overridable components.
  static member inline components (value: obj) = Interop.mkAttr "components" value
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Please use `slotProps`.
  ///
  /// The props used for each component slot.
  static member inline componentsProps (props: IReactProperty list) = Interop.mkAttr "componentsProps" (createObj !!props)
  /// The default value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// CSS media query when `Mobile` mode will be changed to `Desktop`.
  static member inline desktopModeMediaQuery (value: string) = Interop.mkAttr "desktopModeMediaQuery" value
  /// If `true`, the picker and text field are disabled.
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// Do not ignore date part when validating min/max time.
  static member inline disableIgnoringDatePartForTimeValidation (value: bool) = Interop.mkAttr "disableIgnoringDatePartForTimeValidation" value
  /// If `true`, the open picker button will not be rendered (renders only the field).
  static member inline disableOpenPicker (value: bool) = Interop.mkAttr "disableOpenPicker" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Format of the date when rendered in the input(s). Defaults to localized format based on the used `views`.
  static member inline format (value: string) = Interop.mkAttr "format" value
  /// Pass a ref to the `input` element.
  static member inline inputRef (ref: IRefValue<#Element option>) = Interop.mkAttr "inputRef" ref
  /// Pass a ref to the `input` element.
  static member inline inputRef (handler: #Element -> unit) = Interop.mkAttr "inputRef" handler
  /// The label content.
  static member inline label (value: ReactElement) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: seq<ReactElement>) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: string) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (values: string seq) = Interop.mkAttr "label" values
  /// The label content.
  static member inline label (value: int) = Interop.mkAttr "label" value
  /// The label content.
  static member inline label (value: float) = Interop.mkAttr "label" value
  /// Locale for components texts. Allows overriding texts coming from `LocalizationProvider` and `theme`.
  static member inline localeText (value: obj) = Interop.mkAttr "localeText" value
  /// Maximal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline maxTime (value: 'T) = Interop.mkAttr "maxTime" value
  /// Minimal selectable time. The date part of the object will be ignored unless `props.disableIgnoringDatePartForTimeValidation === true`.
  static member inline minTime (value: 'T) = Interop.mkAttr "minTime" value
  /// Step over minutes.
  static member inline minutesStep (value: int) = Interop.mkAttr "minutesStep" value
  /// Step over minutes.
  static member inline minutesStep (value: float) = Interop.mkAttr "minutesStep" value
  /// Callback fired when the value is accepted.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue) => void`
  ///
  /// *value:* The value that was just accepted.
  static member inline onAccept (handler: 'TValue -> unit) = Interop.mkAttr "onAccept" handler
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TValue, context: FieldChangeHandlerContext) => void`
  ///
  /// *value:* The new value.
  ///
  /// *context:* The context containing the validation result of the current value.
  static member inline onChange (handler: 'TValue -> string -> unit) = Interop.mkAttr "onChange" (Func<'TValue, string, unit> handler)
  /// Callback fired when the popup requests to be closed. Use in controlled mode (see `open`).
  static member inline onClose (handler: Event -> unit) = Interop.mkAttr "onClose" handler
  /// Callback fired when the error associated to the current value changes. If the error has a non-null value, then the `TextField` will be rendered in `error` state.
  ///
  /// **Signature:**
  ///
  /// `function(error: TError, value: TValue) => void`
  ///
  /// *error:* The new error describing why the current value is not valid.
  ///
  /// *value:* The value associated to the error.
  static member inline onError (handler: 'TError -> 'TInputValue -> unit) = Interop.mkAttr "onError" (Func<_, _, _> handler)
  /// Callback fired when the popup requests to be opened. Use in controlled mode (see `open`).
  static member inline onOpen (handler: Event -> unit) = Interop.mkAttr "onOpen" handler
  /// Callback fired when the selected sections change.
  ///
  /// **Signature:**
  ///
  /// `function(newValue: FieldSelectedSections) => void`
  ///
  /// *newValue:* The new selected sections.
  static member inline onSelectedSectionsChange (handler: Event -> unit) = Interop.mkAttr "onSelectedSectionsChange" handler
  /// Callback fired on view change.
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The new view.
  static member inline onViewChange (handler: ClockPickerView -> unit) = Interop.mkAttr "onViewChange" handler
  /// Control the popup or dialog open state.
  static member inline open' (value: bool) = Interop.mkAttr "open" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: int) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (value: float) = Interop.mkAttr "selectedSections" value
  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  static member inline selectedSections (endIndex: float, startIndex: float) = Interop.mkAttr "selectedSections" (let x = createEmpty<obj> in x?``endIndex`` <- endIndex; x?``startIndex`` <- startIndex; x)
  /// <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1cw4hi4" focusable="false" aria-hidden="true" viewbox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><br><br>      <path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path><br><br>    </svg>
  ///
  ///     **Deprecated** -
  ///
  ///       Consider using `shouldDisableTime`.
  ///
  ///   Disable specific clock time.
  ///
  /// **Signature:**
  ///
  /// `function(clockValue: number, view: TimeView) => boolean`
  ///
  /// *clockValue:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableClock (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableClock" value
  /// Disable specific time.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate, view: TimeView) => boolean`
  ///
  /// *value:* The value to check.
  ///
  /// *view:* The clock type of the timeValue.
  ///
  /// *returns* (boolean): If `true` the time will be disabled.
  static member inline shouldDisableTime (shouldDisableTime: System.DateTime -> string -> bool) = Interop.mkAttr "shouldDisableTime" (Func<_, _, _> shouldDisableTime)
  /// If `true`, disabled digital clock items will not be rendered.
  static member inline skipDisabled (value: bool) = Interop.mkAttr "skipDisabled" value
  /// The props used for each component slot.
  static member inline slotProps (props: IReactProperty list) = Interop.mkAttr "slotProps" (createObj !!props)
  /// Overridable component slots.
  static member inline slots (value: obj) = Interop.mkAttr "slots" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Amount of time options below or at which the single column time renderer is used.
  static member inline thresholdToRenderTimeInASingleColumn (value: int) = Interop.mkAttr "thresholdToRenderTimeInASingleColumn" value
  /// Amount of time options below or at which the single column time renderer is used.
  static member inline thresholdToRenderTimeInASingleColumn (value: float) = Interop.mkAttr "thresholdToRenderTimeInASingleColumn" value
  /// The time steps between two time unit options. For example, if `timeStep.minutes = 8`, then the available minute options will be `[0, 8, 16, 24, 32, 40, 48, 56]`. When single column time renderer is used, only `timeStep.minutes` will be used.
  static member inline timeSteps (?hours: float, ?minutes: float, ?seconds: float) = Interop.mkAttr "timeSteps" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// Define custom view renderers for each section. If `null`, the section will only have field editing. If `undefined`, internally defined view will be the used.
  static member inline viewRenderers (?hours: Func<obj, obj>, ?meridiem: Func<obj, obj>, ?minutes: Func<obj, obj>, ?seconds: Func<obj, obj>) = Interop.mkAttr "viewRenderers" (let x = createEmpty<obj> in (if hours.IsSome then x?``hours`` <- hours.Value); (if meridiem.IsSome then x?``meridiem`` <- meridiem.Value); (if minutes.IsSome then x?``minutes`` <- minutes.Value); (if seconds.IsSome then x?``seconds`` <- seconds.Value); x)
  /// Available views.
  static member inline views ([<ParamArray>] views: TimePickerView []) = Interop.mkAttr "views" views
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module timePicker =

  /// Density of the format when rendered in the input. Setting `formatDensity` to `"spacious"` will add a space before and after each `/`, `-` and `.` character.
  [<Erase>]
  type formatDensity =
    static member inline dense = Interop.mkAttr "formatDensity" "dense"
    static member inline spacious = Interop.mkAttr "formatDensity" "spacious"

  /// The default visible view. Used when the component view is not controlled. Must be a valid option from `views` list.
  [<Erase>]
  type openTo =
    static member inline hours = Interop.mkAttr "openTo" "hours"
    static member inline meridiem = Interop.mkAttr "openTo" "meridiem"
    static member inline minutes = Interop.mkAttr "openTo" "minutes"
    static member inline seconds = Interop.mkAttr "openTo" "seconds"

  /// Force rendering in particular orientation.
  [<Erase>]
  type orientation =
    static member inline landscape = Interop.mkAttr "orientation" "landscape"
    static member inline portrait = Interop.mkAttr "orientation" "portrait"

  /// The currently selected sections. This prop accept four formats: 1. If a number is provided, the section at this index will be selected. 2. If an object with a `startIndex` and `endIndex` properties are provided, the sections between those two indexes will be selected. 3. If a string of type `FieldSectionType` is provided, the first section with that name will be selected. 4. If `null` is provided, no section will be selected If not provided, the selected sections will be handled internally.
  [<Erase>]
  type selectedSections =
    static member inline all = Interop.mkAttr "selectedSections" "all"
    static member inline day = Interop.mkAttr "selectedSections" "day"
    static member inline hours = Interop.mkAttr "selectedSections" "hours"
    static member inline meridiem = Interop.mkAttr "selectedSections" "meridiem"
    static member inline minutes = Interop.mkAttr "selectedSections" "minutes"
    static member inline month = Interop.mkAttr "selectedSections" "month"
    static member inline seconds = Interop.mkAttr "selectedSections" "seconds"
    static member inline weekDay = Interop.mkAttr "selectedSections" "weekDay"
    static member inline year = Interop.mkAttr "selectedSections" "year"

  /// The visible view. Used when the component view is controlled. Must be a valid option from `views` list.
  [<Erase>]
  type view =
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline meridiem = Interop.mkAttr "view" "meridiem"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline seconds = Interop.mkAttr "view" "seconds"


[<Erase>]
type timePickerToolbar =
  /// Callback called when a toolbar is clicked
  ///
  /// **Signature:**
  ///
  /// `function(view: TView) => void`
  ///
  /// *view:* The view to open
  static member inline onViewChange (handler: Event -> unit) = Interop.mkAttr "onViewChange" handler
  /// className applied to the root component.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// If `true`, show the toolbar even in desktop mode.
  static member inline hidden (value: bool) = Interop.mkAttr "hidden" value
  /// Toolbar date format.
  static member inline toolbarFormat (value: string) = Interop.mkAttr "toolbarFormat" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: ReactElement) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (values: seq<ReactElement>) = Interop.mkAttr "toolbarPlaceholder" values
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: string) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (values: string seq) = Interop.mkAttr "toolbarPlaceholder" values
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: int) = Interop.mkAttr "toolbarPlaceholder" value
  /// Toolbar value placeholder—it is displayed when the value is empty.
  static member inline toolbarPlaceholder (value: float) = Interop.mkAttr "toolbarPlaceholder" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module timePickerToolbar =

  /// Currently visible picker view.
  [<Erase>]
  type view =
    static member inline hours = Interop.mkAttr "view" "hours"
    static member inline meridiem = Interop.mkAttr "view" "meridiem"
    static member inline minutes = Interop.mkAttr "view" "minutes"
    static member inline seconds = Interop.mkAttr "view" "seconds"


[<Erase>]
type yearCalendar =
  /// className applied to the root element.
  static member inline className (value: string) = Interop.mkAttr "className" value
  /// The default selected value. Used when the component is not controlled.
  static member inline defaultValue (value: 'T) = Interop.mkAttr "defaultValue" value
  /// If `true` picker is disabled
  static member inline disabled (value: bool) = Interop.mkAttr "disabled" value
  /// If `true`, disable values after the current date for date components, time for time components and both for date time components.
  static member inline disableFuture (value: bool) = Interop.mkAttr "disableFuture" value
  /// If `true`, today's date is rendering without highlighting with circle.
  static member inline disableHighlightToday (value: bool) = Interop.mkAttr "disableHighlightToday" value
  /// If `true`, disable values before the current date for date components, time for time components and both for date time components.
  static member inline disablePast (value: bool) = Interop.mkAttr "disablePast" value
  /// Maximal selectable date.
  static member inline maxDate (value: 'T) = Interop.mkAttr "maxDate" value
  /// Minimal selectable date.
  static member inline minDate (value: 'T) = Interop.mkAttr "minDate" value
  /// Callback fired when the value changes.
  ///
  /// **Signature:**
  ///
  /// `function(value: TDate) => void`
  ///
  /// *value:* The new value.
  static member inline onChange (handler: Event -> unit) = Interop.mkAttr "onChange" handler
  /// If `true` picker is readonly
  static member inline readOnly (value: bool) = Interop.mkAttr "readOnly" value
  /// The date used to generate the new value when both `value` and `defaultValue` are empty.
  static member inline referenceDate (value: 'T) = Interop.mkAttr "referenceDate" value
  /// Disable specific year.
  ///
  /// **Signature:**
  ///
  /// `function(year: TDate) => boolean`
  ///
  /// *year:* The year to test.
  ///
  /// *returns* (boolean): If `true`, the year will be disabled.
  static member inline shouldDisableYear (value: Func<obj, obj>) = Interop.mkAttr "shouldDisableYear" value
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (styleOverrides: #seq<IStyleAttribute>) = Interop.mkAttr "sx" (createObj !!styleOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (#seq<IStyleAttribute>), ?sm: (#seq<IStyleAttribute>), ?md: (#seq<IStyleAttribute>), ?lg: (#seq<IStyleAttribute>), ?xl: (#seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> !!createObj in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverride: Theme -> #seq<IStyleAttribute>) = Interop.mkAttr "sx" (Helpers.themeStylesOverride themeOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeOverrides: (Theme -> #seq<IStyleAttribute>) []) = Interop.mkAttr "sx" (themeOverrides |> Array.map Helpers.themeStylesOverride)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (breakpointThemeOverrides: (IBreakpointKey * (Theme -> #seq<IStyleAttribute>)) []) = Interop.mkAttr "sx" (Helpers.breakpointThemeStylesOverrides breakpointThemeOverrides)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (?xs: (Theme -> #seq<IStyleAttribute>), ?sm: (Theme -> #seq<IStyleAttribute>), ?md: (Theme -> #seq<IStyleAttribute>), ?lg: (Theme -> #seq<IStyleAttribute>), ?xl: (Theme -> #seq<IStyleAttribute>)) = Interop.mkAttr "sx" (let inline paramValue p = p |> Helpers.themeStylesOverride in let x = createEmpty<obj> in (if xs.IsSome then x?``xs`` <- (paramValue xs.Value)); (if sm.IsSome then x?``sm`` <- (paramValue sm.Value)); (if md.IsSome then x?``md`` <- (paramValue md.Value)); (if lg.IsSome then x?``lg`` <- (paramValue lg.Value)); (if xl.IsSome then x?``xl`` <- (paramValue xl.Value)); x)
  /// The system prop that allows defining system overrides as well as additional CSS styles. See the [`sx` page](https://mui.com/system/getting-started/the-sx-prop/) for more details.
  static member inline sx (themeBreakpointOverrides: (Theme -> (IBreakpointKey * #seq<IStyleAttribute>) list) []) = Interop.mkAttr "sx" (Helpers.themeBreakpointStylesOverrides themeBreakpointOverrides)
  /// Choose which timezone to use for the value. Example: "default", "system", "UTC", "America/New\_York". If you pass values from other timezones to some props, they will be converted to this timezone before being used. See the [timezones documention](https://mui.com/x/react-date-pickers/timezone/) for more details.
  static member inline timezone (value: string) = Interop.mkAttr "timezone" value
  /// The selected value. Used when the component is controlled.
  static member inline value (value: 'T) = Interop.mkAttr "value" value
  /// This component does not support children.
  static member inline children  = UnsupportedProp ()

module yearCalendar =

  /// Years rendered per row.
  [<Erase>]
  type yearsPerRow =
    static member inline _3 = Interop.mkAttr "yearsPerRow" 3
    static member inline _4 = Interop.mkAttr "yearsPerRow" 4
