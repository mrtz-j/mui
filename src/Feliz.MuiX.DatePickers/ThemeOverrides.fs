namespace Feliz.MaterialUI.X

open System
open System.ComponentModel
open Fable.Core
open Fable.Core.JsInterop
open Feliz
open Feliz.MaterialUI

[<AutoOpen; EditorBrowsable(EditorBrowsableState.Never)>]
module themeOverrides =

  module theme =

    module styleOverrides =

      [<Erase>]
      type muiDateCalendar =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiDateCalendar", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateCalendar.styleOverrides.root", createObj !!styles)
        /// Styles applied to the transition group element.
        static member inline viewTransitionContainer(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateCalendar.styleOverrides.viewTransitionContainer", createObj !!styles)

      [<Erase>]
      type muiDateField =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiDateField", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateField.styleOverrides.root", createObj !!styles)

      [<Erase>]
      type muiDatePickerToolbar =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiDatePickerToolbar", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDatePickerToolbar.styleOverrides.root", createObj !!styles)
        /// Styles applied to the title element.
        static member inline title(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDatePickerToolbar.styleOverrides.title", createObj !!styles)

      [<Erase>]
      type muiDateTimeField =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiDateTimeField", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimeField.styleOverrides.root", createObj !!styles)

      [<Erase>]
      type muiDateTimePickerTabs =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiDateTimePickerTabs", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerTabs.styleOverrides.root", createObj !!styles)

      [<Erase>]
      type muiDateTimePickerToolbar =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiDateTimePickerToolbar", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.root", createObj !!styles)
        /// Styles applied to the date container element.
        static member inline dateContainer(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.dateContainer", createObj !!styles)
        /// Styles applied to the time container element.
        static member inline timeContainer(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.timeContainer", createObj !!styles)
        /// Styles applied to the time (except am/pm) container element.
        static member inline timeDigitsContainer(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.timeDigitsContainer", createObj !!styles)
        /// Styles applied to the time container if rtl.
        static member inline timeLabelReverse(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.timeLabelReverse", createObj !!styles)
        /// Styles applied to the separator element.
        static member inline separator(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.separator", createObj !!styles)
        /// Styles applied to the am/pm section.
        static member inline ampmSelection(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.ampmSelection", createObj !!styles)
        /// Styles applied to am/pm section in landscape mode.
        static member inline ampmLandscape(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.ampmLandscape", createObj !!styles)
        /// Styles applied to am/pm labels.
        static member inline ampmLabel(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDateTimePickerToolbar.styleOverrides.ampmLabel", createObj !!styles)

      [<Erase>]
      type muiDayCalendarSkeleton =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiDayCalendarSkeleton", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDayCalendarSkeleton.styleOverrides.root", createObj !!styles)
        /// Styles applied to the week element.
        static member inline week(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDayCalendarSkeleton.styleOverrides.week", createObj !!styles)
        /// Styles applied to the day element.
        static member inline daySkeleton(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDayCalendarSkeleton.styleOverrides.daySkeleton", createObj !!styles)

      [<Erase>]
      type muiDigitalClock =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiDigitalClock", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDigitalClock.styleOverrides.root", createObj !!styles)
        /// Styles applied to the list (by default: MenuList) element.
        static member inline list(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDigitalClock.styleOverrides.list", createObj !!styles)
        /// Styles applied to the list item (by default: MenuItem) element.
        static member inline item(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiDigitalClock.styleOverrides.item", createObj !!styles)

      [<Erase>]
      type muiMonthCalendar =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiMonthCalendar", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiMonthCalendar.styleOverrides.root", createObj !!styles)

      [<Erase>]
      type muiMultiSectionDigitalClock =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiMultiSectionDigitalClock", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiMultiSectionDigitalClock.styleOverrides.root", createObj !!styles)

      [<Erase>]
      type muiPickersActionBar =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiPickersActionBar", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersActionBar.styleOverrides.root", createObj !!styles)
        /// Styles applied to the root element unless `disableSpacing={true}`.
        static member inline spacing(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersActionBar.styleOverrides.spacing", createObj !!styles)

      [<Erase>]
      type muiPickersDay =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiPickersDay", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersDay.styleOverrides.root", createObj !!styles)
        /// Styles applied to the root element if `disableMargin=false`.
        static member inline dayWithMargin(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersDay.styleOverrides.dayWithMargin", createObj !!styles)
        /// Styles applied to the root element if `outsideCurrentMonth=true` and `showDaysOutsideCurrentMonth=true`.
        static member inline dayOutsideMonth(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersDay.styleOverrides.dayOutsideMonth", createObj !!styles)
        /// Styles applied to the root element if `outsideCurrentMonth=true` and `showDaysOutsideCurrentMonth=false`.
        static member inline hiddenDaySpacingFiller(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersDay.styleOverrides.hiddenDaySpacingFiller", createObj !!styles)
        /// Styles applied to the root element if `disableHighlightToday=false` and `today=true`.
        static member inline today(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersDay.styleOverrides.today", createObj !!styles)
        /// State class applied to the root element if `selected=true`.
        static member inline selected(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersDay.styleOverrides.selected", createObj !!styles)
        /// State class applied to the root element if `disabled=true`.
        static member inline disabled(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersDay.styleOverrides.disabled", createObj !!styles)

      [<Erase>]
      type muiPickersLayout =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiPickersLayout", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersLayout.styleOverrides.root", createObj !!styles)
        /// Styles applied to the root element in landscape orientation.
        static member inline landscape(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersLayout.styleOverrides.landscape", createObj !!styles)
        /// Styles applied to the contentWrapper element (which contains the tabs and the view itself).
        static member inline contentWrapper(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersLayout.styleOverrides.contentWrapper", createObj !!styles)
        /// Styles applied to the toolbar.
        static member inline toolbar(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersLayout.styleOverrides.toolbar", createObj !!styles)
        /// Styles applied to the action bar.
        static member inline actionBar(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersLayout.styleOverrides.actionBar", createObj !!styles)
        /// Styles applied to the tabs.
        static member inline tabs(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersLayout.styleOverrides.tabs", createObj !!styles)
        /// Styles applied to the shortcuts container.
        static member inline shortcuts(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersLayout.styleOverrides.shortcuts", createObj !!styles)

      [<Erase>]
      type muiPickersShortcuts =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiPickersShortcuts", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersShortcuts.styleOverrides.root", createObj !!styles)
        /// Styles applied to the root element unless `disablePadding={true}`.
        static member inline padding(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersShortcuts.styleOverrides.padding", createObj !!styles)
        /// Styles applied to the root element if dense.
        static member inline dense(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersShortcuts.styleOverrides.dense", createObj !!styles)
        /// Styles applied to the root element if a `subheader` is provided.
        static member inline subheader(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiPickersShortcuts.styleOverrides.subheader", createObj !!styles)

      [<Erase>]
      type muiTimeClock =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiTimeClock", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimeClock.styleOverrides.root", createObj !!styles)
        /// Styles applied to the arrowSwitcher element.
        static member inline arrowSwitcher(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimeClock.styleOverrides.arrowSwitcher", createObj !!styles)

      [<Erase>]
      type muiTimeField =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiTimeField", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimeField.styleOverrides.root", createObj !!styles)

      [<Erase>]
      type muiTimePickerToolbar =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiTimePickerToolbar", !!values)
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.styleOverrides.root", createObj !!styles)
        static member inline separator(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.styleOverrides.separator", createObj !!styles)
        static member inline hourMinuteLabel(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.styleOverrides.hourMinuteLabel", createObj !!styles)
        static member inline hourMinuteLabelLandscape(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.styleOverrides.hourMinuteLabelLandscape", createObj !!styles)
        static member inline hourMinuteLabelReverse(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.styleOverrides.hourMinuteLabelReverse", createObj !!styles)
        static member inline ampmSelection(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.styleOverrides.ampmSelection", createObj !!styles)
        static member inline ampmLandscape(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.styleOverrides.ampmLandscape", createObj !!styles)
        static member inline ampmLabel(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiTimePickerToolbar.styleOverrides.ampmLabel", createObj !!styles)

      [<Erase>]
      type muiYearCalendar =
        /// Allows to create new variants for Material UI components. These new variants can specify what styles the component should have when that specific variant prop value is applied.
        static member inline variants([<ParamArray>] values: {| props: #seq<IReactProperty>; style: #seq<IStyleAttribute> |} []) : IThemeProp = theme.componentVariants("MuiYearCalendar", !!values)
        /// Styles applied to the root element.
        static member inline root(styles: IStyleAttribute list) : IThemeProp = unbox ("components.MuiYearCalendar.styleOverrides.root", createObj !!styles)
