namespace Feliz.MaterialUI.X

open System.ComponentModel
open Fable.Core
open Feliz
open Feliz.MaterialUI

[<AutoOpen; EditorBrowsable(EditorBrowsableState.Never)>]
module classesProps =

  module dateTimePickerTabs =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)


  module dateTimePickerToolbar =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)
      /// Styles applied to the date container element.
      static member inline dateContainer(className: string) : IReactProperty = unbox ("classes.dateContainer", className)
      /// Styles applied to the time container element.
      static member inline timeContainer(className: string) : IReactProperty = unbox ("classes.timeContainer", className)
      /// Styles applied to the time (except am/pm) container element.
      static member inline timeDigitsContainer(className: string) : IReactProperty = unbox ("classes.timeDigitsContainer", className)
      /// Styles applied to the time container if rtl.
      static member inline timeLabelReverse(className: string) : IReactProperty = unbox ("classes.timeLabelReverse", className)
      /// Styles applied to the separator element.
      static member inline separator(className: string) : IReactProperty = unbox ("classes.separator", className)
      /// Styles applied to the am/pm section.
      static member inline ampmSelection(className: string) : IReactProperty = unbox ("classes.ampmSelection", className)
      /// Styles applied to am/pm section in landscape mode.
      static member inline ampmLandscape(className: string) : IReactProperty = unbox ("classes.ampmLandscape", className)
      /// Styles applied to am/pm labels.
      static member inline ampmLabel(className: string) : IReactProperty = unbox ("classes.ampmLabel", className)


  module dayCalendarSkeleton =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)
      /// Styles applied to the week element.
      static member inline week(className: string) : IReactProperty = unbox ("classes.week", className)
      /// Styles applied to the day element.
      static member inline daySkeleton(className: string) : IReactProperty = unbox ("classes.daySkeleton", className)


  module digitalClock =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)
      /// Styles applied to the list (by default: MenuList) element.
      static member inline list(className: string) : IReactProperty = unbox ("classes.list", className)
      /// Styles applied to the list item (by default: MenuItem) element.
      static member inline item(className: string) : IReactProperty = unbox ("classes.item", className)


  module monthCalendar =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)


  module multiSectionDigitalClock =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)


  module pickersDay =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)
      /// Styles applied to the root element if `disableMargin=false`.
      static member inline dayWithMargin(className: string) : IReactProperty = unbox ("classes.dayWithMargin", className)
      /// Styles applied to the root element if `outsideCurrentMonth=true` and `showDaysOutsideCurrentMonth=true`.
      static member inline dayOutsideMonth(className: string) : IReactProperty = unbox ("classes.dayOutsideMonth", className)
      /// Styles applied to the root element if `outsideCurrentMonth=true` and `showDaysOutsideCurrentMonth=false`.
      static member inline hiddenDaySpacingFiller(className: string) : IReactProperty = unbox ("classes.hiddenDaySpacingFiller", className)
      /// Styles applied to the root element if `disableHighlightToday=false` and `today=true`.
      static member inline today(className: string) : IReactProperty = unbox ("classes.today", className)
      /// State class applied to the root element if `selected=true`.
      static member inline selected(className: string) : IReactProperty = unbox ("classes.selected", className)
      /// State class applied to the root element if `disabled=true`.
      static member inline disabled(className: string) : IReactProperty = unbox ("classes.disabled", className)


  module timeClock =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)
      /// Styles applied to the arrowSwitcher element.
      static member inline arrowSwitcher(className: string) : IReactProperty = unbox ("classes.arrowSwitcher", className)


  module yearCalendar =

    /// Override or extend the styles applied to the component.
    [<Erase>]
    type classes =
      /// Styles applied to the root element.
      static member inline root(className: string) : IReactProperty = unbox ("classes.root", className)

