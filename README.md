# Material UI

[Feliz](https://github.com/Zaid-Ajaj/Feliz)-style Fable bindings for [MUI (aka Material-UI)](https://mui.com).

## Build & run Project

Navigate to the project root and run the following commands:

```bash
dotnet fsi build.fsx -- -p watch
```

which will start the docs on `localhost:8080`.

Or alternatively run 

```bash
dotnet fsi build.fsx -- -p build
```

to build and package the Packages.