{
  description = "MUI";

  inputs = {
    flake-schemas.url = "https://flakehub.com/f/DeterminateSystems/flake-schemas/*.tar.gz";
    nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/0.1.*.tar.gz";
  };

  outputs = {
    self,
    flake-schemas,
    nixpkgs,
  }: let
    supportedSystems = ["x86_64-linux" "aarch64-darwin"];
    forEachSupportedSystem = f:
      nixpkgs.lib.genAttrs supportedSystems (system:
        f {
          pkgs = import nixpkgs {inherit system;};
        });
  in {
    schemas = flake-schemas.schemas;

    devShells = forEachSupportedSystem ({pkgs}: {
      default = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          dotnet-sdk_8
          fsautocomplete
          bun
        ];
        DOTNET_ROOT = "${pkgs.dotnet-sdk_8}";
        DOTNET_CLI_TELEMETRY_OPTOUT = "0";
        VITE_PLUGIN_FABLE_DEBUG = "1";
      };
    });
  };
}
