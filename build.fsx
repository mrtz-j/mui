#r "nuget: Fun.Build, 1.1.2"
#r "nuget: Fake.IO.FileSystem, 6.0.0"

open System.IO
open Fake.IO
open Fun.Build

let solutionFile = "mui.sln"
let deployDir = Path.getFullName "deploy"
let reportDir = Path.getFullName "reports"
let docsPath = Path.getFullName "docs-old/"

let restoreStage =
    stage "Restore" {
        run "dotnet tool restore"
        run $"dotnet restore --locked-mode %s{solutionFile}"
    }

let clean (input: string seq) =
    async {
        input
        |> Seq.iter (fun dir ->
            if Directory.Exists(dir) then
                Directory.Delete(dir, true)
        )
    }

pipeline "Bundle" {
    workingDir __SOURCE_DIRECTORY__
    restoreStage
    stage "Clean" {run (clean [|"dist"; "reports"|])}
    stage "CheckFormat" {run "dotnet fantomas docs-old build.fsx --check"}

    stage "Build" {run "dotnet fable --noCache -s -o .build --run vite build -c ../vite.config.js"}

    runIfOnlySpecified true
}

pipeline "Watch" {
    workingDir __SOURCE_DIRECTORY__
    stage "Clean" {run (clean [|"dist"; "reports"|])}
    restoreStage
    stage "Docs" {
        workingDir docsPath
        run "dotnet fable watch -s -o .build --run bunx --bun vite -c ../vite.config.js"
    }

    runIfOnlySpecified false
}

pipeline "Build" {
    workingDir __SOURCE_DIRECTORY__
    stage "Clean" {run (clean [|"dist"; "reports"|])}
    restoreStage
    stage "CheckFormat" {run "dotnet fantomas docs-old build.fsx --check"}
    stage "Build" {run "dotnet build -c Release --tl"}
    stage "Pack" {run "dotnet pack --no-restore -c Release --tl"}

    runIfOnlySpecified true
}

pipeline "Analyze" {
    workingDir __SOURCE_DIRECTORY__
    stage "Report" {run $"dotnet msbuild /t:AnalyzeSolution %s{solutionFile}"}

    // post [
    //     stage "Sarif" {
    //         workingDir reportDir
    //         run
    //             "./sarif-converter --type codequality ProvisionService.sarif CostService.sarif ../sarif-report.json"
    //     }
    // ]

    runIfOnlySpecified true
}

pipeline "Format" {
    workingDir __SOURCE_DIRECTORY__
    stage "Restore" {run "dotnet tool restore"}
    stage "Fantomas" {run "dotnet fantomas docs-old build.fsx"}
    runIfOnlySpecified true
}

pipeline "Sign" {
    workingDir __SOURCE_DIRECTORY__
    stage "Restore" {run "dotnet tool restore"}

    // stage "Main" {
    //     // NOTE: Could this be parallelized?
    //     run
    //         "dotnet telplin src/ProvisionService/ProvisionService.fsproj -- /p:Configuration=Release"
    //     run "dotnet telplin src/CostService/CostService.fsproj -- /p:Configuration=Release"
    // }

    runIfOnlySpecified true
}

tryPrintPipelineCommandHelp ()
