import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'
import { dependencies } from './package.json'

const vendorDeps = ['react', 'react-dom']

const chunksFromDeps = (deps, vendorDeps) => {
    const chunks = {}
    Object.keys(deps).forEach((key) => {
        if (vendorDeps.includes(key) || key.startsWith('@mui')) {
            return
        }
        chunks[key] = [key]
    })
    return chunks
}

export default defineConfig({
    plugins: [react()],
    clearScreen: false,
    // base: '/mui/docs-old',
    root: '.',
    publicDir: "./public",
    assetsInclude: "./**/*.md",
    build: {
        outDir: "../deploy/public",
        emptyOutDir: true,
        sourcemap: true,
        rollupOptions: {
            input: {
                main: path.resolve(__dirname, './docs-old/index.html')
            },
            output: {
                manualChunks: {
                    vendor: vendorDeps,
                    ...chunksFromDeps(dependencies, vendorDeps)
                },
                entryFileNames: 'js/main.min.js',
                chunkFileNames: 'js/[name].min.js',
                assetFileNames: '[ext]/[name].[ext]'
            },
        },
    },
    server: {
        watch: {
            ignored: [
                "**/*.fs" // Don't watch F# files
            ]
        },
        host: '0.0.0.0',
        port: 8080,
        open: true
    },
});
