module Samples.Samples.DateTimePickers.DateTimePickers

open System
open Feliz
open Feliz.MaterialUI
open Feliz.MaterialUI.X

[<ReactComponent>]
let DateTimePickersSample (key: string) =

    let dtValue, setDtValue = React.useState<DateTime option> (Some DateTime.Now)

    MuiX.localizationProvider [
        localizationProvider.dateAdapter.dateFns
        prop.children [
            Mui.container [
                prop.children [
                    MuiX.datePicker [
                        prop.key key
                        dateTimePicker.ampm false
                        dateTimePicker.format "dd.MM.yyyy HH:mm"
                        dateTimePicker.onChange (fun dt _ -> setDtValue dt)
                        dateTimePicker.slotProps [textField.variant.outlined]
                        dateTimePicker.value dtValue
                    ]
                ]
            ]
        ]
    ]
