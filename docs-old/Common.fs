[<AutoOpen>]
module Common

module Constants =
    [<Literal>]
    let SourceRepoUrl = "https://gitlab.com/serit/fable/serit.feliz.materialui"

    [<Literal>]
    let SourceRepoEditUrl = SourceRepoUrl + "/edit/main"

    [<Literal>]
    let SourceRepoDocsAppArticleDirEditUrl = SourceRepoEditUrl + "/docs-app/public/"
