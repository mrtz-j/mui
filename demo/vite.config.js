import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import fable from "vite-plugin-fable";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    // See `jsx` option from https://esbuild.github.io/api/#transformation
    fable({ jsx: "automatic" }),
    react({ include: /\.(fs|js|jsx|ts|tsx)$/ }),
  ],
});
