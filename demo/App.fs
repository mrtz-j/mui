module App

open Fable.Core
open Browser
open Feliz


[<JSX.Component>]
let App () = SignUp.View ()

let root = ReactDOM.createRoot (document.getElementById "root")
root.render (App() |> toReact)
